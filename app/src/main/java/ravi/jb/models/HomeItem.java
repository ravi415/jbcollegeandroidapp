package ravi.jb.models;

import android.graphics.drawable.Drawable;

/**
 * Created by brakisho on 2/18/2018.
 */

public class HomeItem {

    private String name;
    private int order;
    private Drawable drawable;
    private Class clazz;



    public HomeItem(){

    }
    public HomeItem(String name,int order,Drawable drawable,Class clazz){
        this.name=name;
        this.order=order;
        this.drawable=drawable;
        this.clazz=clazz;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getOrder() {
        return order;
    }

    public void setOrder(int order) {
        this.order = order;
    }

    public Drawable getDrawable() {
        return drawable;
    }

    public void setDrawable(Drawable drawable) {
        this.drawable = drawable;
    }

    public Class getClazz() {
        return clazz;
    }

    public void setClazz(Class clazz) {
        this.clazz = clazz;
    }
}
