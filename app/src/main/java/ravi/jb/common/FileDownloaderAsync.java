package ravi.jb.common;

import android.app.Activity;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.FileProvider;
import android.util.Log;
import android.webkit.MimeTypeMap;
import android.widget.Toast;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Random;

import ravi.jb.BuildConfig;
import ravi.jb.util.CommonUtil;

/**
 * Created by Endrjeet on 4/14/2017.
 */
public class FileDownloaderAsync extends AsyncTask<String, Void, Void> {

    private static final String TAG=FileDownloaderAsync.class.getSimpleName();

    private static final int  MEGABYTE = 1024 * 1;

    int progress = 0;
    NotificationCompat.Builder notification;
    NotificationManager notificationManager;
    int dataGot=0;
    int id=93;
    private Context context;
    private  File pdfFile;


    private FileDownloaderAsync(){

        Random random=new Random();
        id=random.nextInt(133)+7;
    }

    public static FileDownloaderAsync newInstance(Context context){
        FileDownloaderAsync fileDownloaderAsync= new FileDownloaderAsync();
        fileDownloaderAsync.context=context;
        return  fileDownloaderAsync;
    }

    protected void onPreExecute() {


        notificationManager =
                (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        notification  = new NotificationCompat.Builder(context);
        notification.setContentTitle("File Download")
                .setContentText("Download in progress")
                .setSmallIcon(android.R.drawable.sym_contact_card);

        notificationManager.notify(id, notification.build());


    }
    @Override
    protected Void doInBackground(String... strings) {

        try {
            String fileUrl = strings[0];
            String fileName = strings[1];

            pdfFile = new File(CommonUtil.getExternalStorageFolder(), fileName);

            try {
                pdfFile.createNewFile();

                showToastMessage("Download started");
                Log.d(TAG, "Downloading path:" + pdfFile.getAbsolutePath());

                downloadFile(fileUrl, pdfFile);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }catch(Exception e){
            Log.e(TAG,e.getMessage()+"");
        }
        return null;
    }

    private void showToastMessage(final String msg){

        ((Activity)context).runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(context,msg,Toast.LENGTH_SHORT).show();
            }
        });
    }

    public  void downloadFile(String fileUrl, File directory){
        try {

            URL url = new URL(fileUrl);
            HttpURLConnection urlConnection = (HttpURLConnection)url.openConnection();
            //urlConnection.setRequestMethod("GET");
            //urlConnection.setDoOutput(true);
            urlConnection.connect();

            InputStream inputStream = urlConnection.getInputStream();
            FileOutputStream fileOutputStream = new FileOutputStream(directory);
            int totalSize = urlConnection.getContentLength();

            Log.d(TAG,"Going to download:"+fileUrl+" file size:"+totalSize);
            byte[] buffer = new byte[MEGABYTE];
            int byteRead = 0;
            while((byteRead = inputStream.read(buffer))>0 ){
                fileOutputStream.write(buffer, 0, byteRead);
                dataGot=dataGot+byteRead;

                if(dataGot>0) {
                    this.progress = (int) ((dataGot / (float) totalSize) * 100);
                    this.onProgressUpdate();
                }
                //Log.d(TAG,"Updating progress:"+this.progress+"::dataGot::"+dataGot);
            }
            fileOutputStream.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    @Override
    protected void onProgressUpdate(Void... values) {

        notification.setProgress(100, this.progress, false);
        // Displays the progress bar for the first time.


        ((Activity)context).runOnUiThread(new Runnable() {
            @Override
            public void run() {
                notificationManager.notify(id, notification.build());
            }
        });

    }

    protected void onPostExecute(Void result) {


try {


    Uri fileUri = FileProvider.getUriForFile(context, BuildConfig.APPLICATION_ID + ".provider", pdfFile);
    MimeTypeMap mime = MimeTypeMap.getSingleton();
    String ext=pdfFile.getName().substring(pdfFile.getName().indexOf(".") + 1);
    String type = mime.getMimeTypeFromExtension(ext);
    Intent openFile = new Intent(Intent.ACTION_VIEW, fileUri);
    openFile.setDataAndType(fileUri, type);
    openFile.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
    PendingIntent p = PendingIntent.getActivity(context, 0, openFile, 0);

    showToastMessage("Download complete");
    notification.setContentText("Download complete")
            .setContentIntent(p)
            .setProgress(0, 0, false);

    Log.d(TAG,"Searching for file:"+fileUri+"::while:"+pdfFile);
    notificationManager.notify(id, notification.build());

}catch (Exception e){
    Log.e(TAG,e.getMessage());
    e.printStackTrace();
}
    }

}