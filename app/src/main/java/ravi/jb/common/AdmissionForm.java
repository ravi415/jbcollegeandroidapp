package ravi.jb.common;

import android.app.Activity;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Environment;
import android.support.v4.app.NotificationCompat;
import android.webkit.MimeTypeMap;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created by Endrjeet on 4/14/2017.
 */
public class AdmissionForm extends AsyncTask<String, Void, Void> {

    private static final int  MEGABYTE = 1024 * 1;

    int progress = 0;
    NotificationCompat.Builder notification;
    NotificationManager notificationManager;
    int dataGot=0;
    int id=93;
    private Context context;
    private  File pdfFile;


    private AdmissionForm(){

    }

    public static AdmissionForm newInstance(Context context){
        AdmissionForm fileDownloaderAsync= new AdmissionForm();
        fileDownloaderAsync.context=context;
        return  fileDownloaderAsync;
    }

    protected void onPreExecute() {



        notificationManager =
                (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        notification  = new NotificationCompat.Builder(context);
        notification.setContentTitle("File Download")
                .setContentText("Download in progress")
                .setSmallIcon(android.R.drawable.sym_contact_card);


        notificationManager.notify(id, notification.build());

    }
    @Override
    protected Void doInBackground(String... strings) {
        String fileUrl = strings[0];
        String fileName = strings[1];
        String extStorageDirectory = Environment.getExternalStorageDirectory().toString();
        File folder = new File(extStorageDirectory, "JB-College");
        folder.mkdir();

        pdfFile = new File(folder, fileName);

        try{
            pdfFile.createNewFile();
        }catch (IOException e){
            e.printStackTrace();
        }
        downloadFile(fileUrl, pdfFile);
        return null;
    }

    public  void downloadFile(String fileUrl, File directory){
        try {

            URL url = new URL(fileUrl);
            HttpURLConnection urlConnection = (HttpURLConnection)url.openConnection();
            //urlConnection.setRequestMethod("GET");
            //urlConnection.setDoOutput(true);
            urlConnection.connect();

            InputStream inputStream = urlConnection.getInputStream();
            FileOutputStream fileOutputStream = new FileOutputStream(directory);
            int totalSize = urlConnection.getContentLength();

            byte[] buffer = new byte[MEGABYTE];
            int byteRead = 0;
            while((byteRead = inputStream.read(buffer))>0 ){
                fileOutputStream.write(buffer, 0, byteRead);
                dataGot=dataGot+byteRead;
                this.progress = (int) ((dataGot / (float) totalSize) * 100);
                this.onProgressUpdate();

            }
            fileOutputStream.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    @Override
    protected void onProgressUpdate(Void... values) {

        notification.setProgress(100, this.progress, false);
        // Displays the progress bar for the first time.


        ((Activity)context).runOnUiThread(new Runnable() {
            @Override
            public void run() {
                notificationManager.notify(id, notification.build());
            }
        });

    }

    protected void onPostExecute(Void result) {



        MimeTypeMap mime = MimeTypeMap.getSingleton();
        String ext=pdfFile.getName().substring(pdfFile.getName().indexOf(".") + 1);
        String type = mime.getMimeTypeFromExtension(ext);
        Intent openFile = new Intent(Intent.ACTION_VIEW, Uri.fromFile(pdfFile));
        openFile.setDataAndType(Uri.fromFile(pdfFile), type);
        PendingIntent p = PendingIntent.getActivity(context, 0, openFile, 0);

        notification.setContentText("Download complete")
                .setContentIntent(p)
                .setProgress(0, 0, false);

        notificationManager.notify(id, notification.build());
    }

}