package ravi.jb.adapters;

import android.app.Activity;
import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.squareup.picasso.Picasso;

import java.util.List;

import ravi.jb.R;
import ravi.jb.util.CommonUtil;

public class ImageAdapter extends BaseAdapter {

    private Context context;
    private LayoutInflater inflater;
    private List<String> urls;
    private ProgressBar progressBar;


    public ImageAdapter(Context context,List<String> urls,ProgressBar progressBar) {
        this.context = context;
        this.progressBar=progressBar;
        this.urls=urls;
         inflater= ((Activity)context).getLayoutInflater();
    }


    // returns the number of images
    public int getCount() {
        return urls.size();
    }

    // returns the ID of an item
    public Object getItem(int position) {
        return urls.get(position);
    }

    // returns the ID of an item
    public long getItemId(int position) {
        return position;
    }

    // returns an ImageView view
    public View getView(final int position, final View convertView, ViewGroup parent) {


        View vi = convertView;
        if(vi==null){
            vi = inflater.inflate(R.layout.image_item,null);
           }
        final ImageView imageView=(ImageView)vi.findViewById(R.id.imgItem);
        try {

            if (!TextUtils.isEmpty(urls.get(position))) {
                Picasso.Builder picassoBuilder = new Picasso.Builder(((Activity) context));
         /*   picassoBuilder.listener(new Picasso.Listener() {
                @Override
                public void onImageLoadFailed(Picasso picasso, Uri uri, Exception exception) {
                    Log.e("PICASSO", uri.toString(), exception);
                }
            });*/
                Picasso picasso = picassoBuilder.build();
                picasso.load(urls.get(position))
                        .resize((int) context.getResources().getDimension(R.dimen.gImgW),
                                (int) context.getResources().getDimension(R.dimen.gImgH))
                        .into(imageView, new com.squareup.picasso.Callback() {
                            @Override
                            public void onSuccess() {
                                imageView.setVisibility(View.VISIBLE);
                            }

                            @Override
                            public void onError() {
                                imageView.setVisibility(View.GONE);
                                /*if(CommonUtil.isNetworkOnline(context))
                                    CommonUtil.showOfflineMsg(context);
                                else
                                    CommonUtil.showToastMessage(context,"Error in data get!");*/
                            }
                        });
            }

            }catch(Exception e){

            }

         progressBar.setVisibility(View.GONE);
        return vi;
    }
}