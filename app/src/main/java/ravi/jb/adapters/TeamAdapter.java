package ravi.jb.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import ravi.jb.R;
import ravi.jb.events.model.TeamMember;

/**
 * Created by brakisho on 2/9/2017.
 */

public class TeamAdapter extends RecyclerView.Adapter<TeamAdapter.MyViewHolder> {

    private static final String TAG= ravi.jb.events.adapters.RecyclerEventAdapter.class.getSimpleName();

    private List<TeamMember> members;
    private Context context;
    private LayoutInflater inflater;

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public TextView name,about;
        public LinearLayout emails;
        public LinearLayout personalLinks;
        public ImageView imgView;
        public MyViewHolder(View view) {
            super(view);
            name = (TextView) view.findViewById(R.id.name);
            about = (TextView) view.findViewById(R.id.about);
            emails = (LinearLayout) view.findViewById(R.id.emails);
            personalLinks = (LinearLayout) view.findViewById(R.id.personalLinks);
            imgView=(ImageView) view.findViewById(R.id.icon);
            view.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
        }
    }


    public TeamAdapter(List<TeamMember> events,Context context) {
        this.members = events;
        this.context=context;
        this.inflater=(LayoutInflater) context.getSystemService( Context.LAYOUT_INFLATER_SERVICE );
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.team_info_row, parent, false);

        return new MyViewHolder(itemView);


    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {

        final TeamMember member = members.get(position);

        String eventName=member.getName();

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            holder.name.setText(Html.fromHtml(eventName, Html.FROM_HTML_MODE_LEGACY));
        } else {
            holder.name.setText(Html.fromHtml(eventName));
        }


        String desc=member.getAbout();

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            holder.about.setText(Html.fromHtml(desc, Html.FROM_HTML_MODE_LEGACY));
        } else {
            holder.about.setText(Html.fromHtml(desc));
        }


        if(holder.imgView!=null){

            if(member!=null && member.getImgUrl()!=null && !TextUtils.isEmpty(member.getImgUrl())) {
                Log.d(TAG,"Getting:"+member.getImgUrl());
                ((Activity) context).runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Picasso picasso = Picasso.with(context);
                        picasso.load(member.getImgUrl())
                                .resize(80, 80)
                                .into(holder.imgView);
                    }
                });
            }
            else
                holder.imgView.setImageResource(R.drawable.user48);
        }
        else{
            holder.imgView.setImageResource(R.drawable.user48);
        }


        if(member!=null && member.getEmails()!=null &&  member.getEmails().size()>0)
        {
            TextView textView =null;
            for(String e:member.getEmails()) {
                textView = (TextView) inflater.inflate(R.layout.clickable_text_view,null);
                textView.setTag(e);
                textView.setText(e);
                textView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Object obj=view.getTag();
                        Log.d(TAG,"email click listener:"+obj);
                        if(obj instanceof String){
                            String s=(String)obj;

                            Intent intent = new Intent(Intent.ACTION_SEND);
                            intent.setType("plain/text");
                            intent.putExtra(android.content.Intent.EXTRA_EMAIL, new String[] {s});
                            intent.putExtra(android.content.Intent.EXTRA_SUBJECT,"");
                            intent.putExtra(android.content.Intent.EXTRA_TEXT, "");

                            /* Send it off to the Activity-Chooser */
                            context.startActivity(Intent.createChooser(intent,"Send"));

                        }
                    }
                });

                holder.emails.addView(textView);
            }
        }
        if(member!=null && member.getPersonalLinks()!=null &&  member.getPersonalLinks().size()>0)
        {
            TextView textView =null;
            for(String e:member.getPersonalLinks()) {
                textView = (TextView) inflater.inflate(R.layout.clickable_text_view,null);
                textView.setTag(e);
                textView.setText(e);
                textView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Object obj=view.getTag();
                        Log.d(TAG,"another click listener:"+obj);
                        if(obj instanceof String){
                            String s=(String)obj;

                            Intent sendIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(s));
                            Intent chooser = Intent.createChooser(sendIntent, "Open with");
                            context.startActivity(chooser);
                        }
                    }
                });
                holder.personalLinks.addView(textView);
            }
        }

        Log.d(TAG,"Set:"+member);
    }



    @Override
    public int getItemCount() {
        return members.size();
    }


}