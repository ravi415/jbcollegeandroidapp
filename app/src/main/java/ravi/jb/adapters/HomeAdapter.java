package ravi.jb.adapters;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import ravi.jb.JbApplication;
import ravi.jb.R;
import ravi.jb.models.HomeItem;

/**
 * Created by brakisho on 2/9/2017.
 */

public class HomeAdapter extends RecyclerView.Adapter<HomeAdapter.MyViewHolder> {

    private static final String TAG=HomeAdapter.class.getSimpleName();
    private List<HomeItem> homeItems;
    private int colors[]=null;
    private Context context;


    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public TextView name;
        public ImageView icon;
        public View rootView;
        public MyViewHolder(View view) {
            super(view);
            name = (TextView) view.findViewById(R.id.name);
            icon=(ImageView)view.findViewById(R.id.icon);
            rootView=view;
            view.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            int position=getAdapterPosition();
            HomeItem homeItem=homeItems.get(position);

            Class aClass=homeItem.getClazz();
            if(aClass!=null){
                Intent intent=new Intent(context,aClass);
                context.startActivity(intent);
            }
            else if(context.getString(R.string.check_result_item).equalsIgnoreCase(homeItem.getName())) {
                if(!TextUtils.isEmpty(JbApplication.configs.get("mdu.result.uri")))
                    context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(JbApplication.configs.get("mdu.result.uri"))));
            }
        }
    }


    public HomeAdapter(List<HomeItem> homeItems, Context context) {
        this.homeItems = homeItems;
        this.context=context;
        colors=context.getResources().getIntArray(R.array.colorArray);
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.home_item_row, parent, false);

            return new MyViewHolder(itemView);


    }


    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {

        final HomeItem homeItem = homeItems.get(position);

        String name=homeItem.getName();

            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
                holder.name.setText(Html.fromHtml(name, Html.FROM_HTML_MODE_LEGACY));
            } else {
                holder.name.setText(Html.fromHtml(name));
            }

        holder.icon.setImageDrawable(homeItem.getDrawable());
        holder.icon.setVisibility(View.VISIBLE);
        holder.rootView.setBackgroundColor(colors[position%colors.length]);

    }

    @Override
    public int getItemCount() {
        return homeItems.size();
    }


}