package ravi.jb.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import ravi.jb.R;
import ravi.jb.util.CommonUtil;

/**
 * Created by brakisho on 2/9/2017.
 */

public class ImageSlidingAdapter extends RecyclerView.Adapter<ImageSlidingAdapter.MyViewHolder> {

    private static final String TAG=ImageSlidingAdapter.class.getSimpleName();
    private String imgUrls[];
    private Context context;


    public class MyViewHolder extends RecyclerView.ViewHolder {
        public ImageView imageView;
        public View rootView;
        public MyViewHolder(View view) {
            super(view);
            imageView=view.findViewById(R.id.imageView);
            rootView=view;
        }
    }


    public ImageSlidingAdapter(String imgUrls[], Context context) {
        this.imgUrls = imgUrls;
        this.context=context;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.image_sliding_row, parent, false);

            return new MyViewHolder(itemView);


    }


    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {

            setImageView(holder,position);
    }

    private void setImageView(final MyViewHolder holder,int pos){

        if(pos<0 ||imgUrls==null || pos>=imgUrls.length)
            return;

        try {

            if(!TextUtils.isEmpty(imgUrls[pos])) {
                Picasso.Builder picassoBuilder = new Picasso.Builder(context);
                Picasso picasso = picassoBuilder.build();

                picasso.load(imgUrls[pos])
                        .into(holder.imageView, new com.squareup.picasso.Callback() {
                            @Override
                            public void onSuccess() {
                                holder.imageView.setVisibility(View.VISIBLE);
                            }

                            @Override
                            public void onError() {
                                holder.imageView.setVisibility(View.GONE);
                                if(CommonUtil.isNetworkOnline(context))
                                    CommonUtil.showOfflineMsg(context);
                                else
                                    CommonUtil.showToastMessage(context,"Error in data get!");
                            }
                        });

            }

        }catch(Exception e){
        }
    }

    @Override
    public int getItemCount() {
        return imgUrls.length;
    }


}