package ravi.jb;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.messaging.FirebaseMessaging;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import ravi.jb.events.model.NetworkChangeEvent;
import ravi.jb.fcm.RegistrationIntentService;
import ravi.jb.fragments.AdmissionFragment;
import ravi.jb.fragments.AmenitiesFragment;
import ravi.jb.fragments.ContactusFragment;
import ravi.jb.fragments.DownloadFragment;
import ravi.jb.fragments.FeeFragment;
import ravi.jb.fragments.HomeFragment;
import ravi.jb.fragments.TeamInfoFragment;
import ravi.jb.fragments.TieupsFragment;
import ravi.jb.util.CommonUtil;


public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {


    private static final String TAG=MainActivity.class.getSimpleName();

    private Toolbar toolbar;
    private TextView toolbar_title;
    private NavigationView navigationView;
    private FragmentManager fragmentManager = null;

    private static final int EXTERNAL_STORAGE_PERMISSION_CONSTANT = 100;
    private static final int REQUEST_PERMISSION_SETTING = 101;
    private boolean sentToSettings = false;
    private SharedPreferences permissionStatus;
    private boolean doubleBackToExitPressedOnce=false;
    private MenuItem homeItem,refreshItem;
    private DownloadFragment downloadFragment;
    private Snackbar snackbar;
    private ImageView offlineIV;
    private TextView impMsgTV;
    private LinearLayout impMsgLL;


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(NetworkChangeEvent event)
    {
        if(event.isOnline()&& offlineIV!=null) {
            offlineIV.setVisibility(View.GONE);
            CommonUtil.showToastMessage(this,this.getResources().getString(R.string.usr_online_msg));
        }
        else if(offlineIV!=null){
            offlineIV.setVisibility(View.VISIBLE);
            CommonUtil.showToastMessage(this,this.getResources().getString(R.string.usr_offline_msg));
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        fragmentManager = getSupportFragmentManager();

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar_title=(TextView) toolbar.findViewById(R.id.toolbar_title);
        setSupportActionBar(toolbar);
        permissionStatus = getSharedPreferences("permissionStatus",MODE_PRIVATE);
        offlineIV=(ImageView)findViewById(R.id.offlineIV);

        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayShowHomeEnabled(true);
        actionBar.setIcon(R.mipmap.jb);
        actionBar.setTitle("");

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        MenuItem item = navigationView.getMenu().findItem(R.id.nav_home);
        onNavigationItemSelected(item);

        ImageView headerImg=(ImageView) (navigationView.getHeaderView(0)).findViewById(R.id.navHeadImg);

        /*headerImg.setColorFilter(getResources().getColor(R.color.teal500),
                PorterDuff.Mode.MULTIPLY);*/

        Log.d(TAG,"Bg work");
        Intent intent = new Intent(this, RegistrationIntentService.class);
        startService(intent);
        FirebaseMessaging.getInstance().subscribeToTopic("jbnews");
        startBgWork(this);

    }


    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {

            if (doubleBackToExitPressedOnce) {
                super.onBackPressed();
                return;
            }

            this.doubleBackToExitPressedOnce = true;
            showSnackbarMessage(getResources().getString(R.string.exit_msg));

            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    doubleBackToExitPressedOnce=false;
                }
            }, 3000);
        }
    }

    public void showSnackbarMessage(String message){
        showSnackbarMessage(message,100,false);
    }
    public void showSnackbarMessage(String message,boolean isInfinite) {
        showSnackbarMessage(message,5,isInfinite);
    }


    public void showSnackbarMessage(String message,int duration,boolean isInfinite){
        snackbar=Snackbar.make(findViewById(R.id.drawer_layout),message, Snackbar.LENGTH_LONG);
        // get snackbar view
        View mView = snackbar.getView();
        // get textview inside snackbar view
        TextView mTextView = (TextView) mView.findViewById(android.support.design.R.id.snackbar_text);
        // set text to center
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1)
            mTextView.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
        else
            mTextView.setGravity(Gravity.CENTER_HORIZONTAL);
        if(isInfinite)
            snackbar.setDuration(Snackbar.LENGTH_INDEFINITE);
        else
            snackbar.setDuration(duration);

        snackbar.show();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
            getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == R.id.action_share) {
            launchSendIntent();
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.

        if (item.isChecked())
            item.setChecked(false);
        else
            item.setChecked(true);

        int id = item.getItemId();

        String title=item.getTitle().toString();


        Fragment fragment = null;
        switch (id) {
            case R.id.nav_home:
                fragment = HomeFragment.newInstance();
                break;
            case R.id.nav_addmission:
                fragment = AdmissionFragment.newInstance();
                break;
            case R.id.nav_manage:
                fragment = FeeFragment.newInstance();
                break;
            case R.id.nav_tie:
                fragment = TieupsFragment.newInstance();
                break;
            case R.id.nav_amen:
                fragment = AmenitiesFragment.newInstance();
                break;
            case R.id.nav_contact:
                fragment = ContactusFragment.newInstance();
                break;
            case R.id.nav_download:
                downloadFragment = DownloadFragment.newInstance();
                fragment=downloadFragment;
                break;
            case R.id.nav_devs:
                fragment = TeamInfoFragment.newInstance();
                break;
            default:
                break;
        }

        invokeFragment(fragment,title);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void launchSendIntent(){
        try {
            Intent i = new Intent(Intent.ACTION_SEND);
            i.setType("text/plain");
            i.putExtra(Intent.EXTRA_SUBJECT,getResources().getString(R.string.app_name));
            String sAux =getResources().getString(R.string.share_msg);
            i.putExtra(Intent.EXTRA_TEXT, sAux);
            startActivity(Intent.createChooser(i,getResources().getString(R.string.share_select)));
        } catch(Exception e) {
            //e.toString();
        }

    }


    private void invokeFragment(Fragment fragment,String title){
        if (fragment != null) {
            fragment=getNetworkFragment(fragment);
           startFragment(fragment, title);
        }
    }

    public void startFragment(Fragment fragment,String title){
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.frame, fragment);
        fragmentTransaction.commitAllowingStateLoss();
        toolbar_title.setText(title);

    }

    private Fragment getNetworkFragment(Fragment fragment){
        return fragment;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == EXTERNAL_STORAGE_PERMISSION_CONSTANT) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                //The External Storage Write Permission is granted to you... Continue your left job...
                downloadFragment.invokeAsync();
            } else {
                if (ActivityCompat.shouldShowRequestPermissionRationale(MainActivity.this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                    //Show Information about why you need the permission
                    AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                    builder.setTitle("Need Storage Permission");
                    builder.setMessage("This app needs storage permission");
                    builder.setPositiveButton("Grant", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();


                            ActivityCompat.requestPermissions(MainActivity.this, new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, EXTERNAL_STORAGE_PERMISSION_CONSTANT);


                        }
                    });
                    builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();
                        }
                    });
                    builder.show();
                } else {
                    Toast.makeText(getBaseContext(),"Unable to get Permission",Toast.LENGTH_LONG).show();
                }
            }
        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_PERMISSION_SETTING) {
            if (ActivityCompat.checkSelfPermission(MainActivity.this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    == PackageManager.PERMISSION_GRANTED) {
                //Got Permission
                downloadFragment.invokeAsync();
            }
        }
    }


/*    @Override
    protected void onPostResume() {
        super.onPostResume();
        if (sentToSettings) {
            if (ActivityCompat.checkSelfPermission(MainActivity.this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                //Got Permission
                proceedAfterPermission();
            }
        }
    }*/

    public  void startBgWork(MainActivity mainActivity){

    }

}