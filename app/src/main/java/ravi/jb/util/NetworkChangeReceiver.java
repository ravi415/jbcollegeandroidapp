package ravi.jb.util;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

import org.greenrobot.eventbus.EventBus;

import ravi.jb.events.model.NetworkChangeEvent;

/**
 * Created by brakisho on 11/15/2017.
 */

public class NetworkChangeReceiver extends BroadcastReceiver {

    private static final String TAG=NetworkChangeReceiver.class.getSimpleName();

    @Override
    public void onReceive(final Context context, final Intent intent) {
        final ConnectivityManager connMgr = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo networkInfo=connMgr.getActiveNetworkInfo();
        if (networkInfo!=null) {
            Log.d(TAG, "Online now");
            EventBus.getDefault().postSticky(new NetworkChangeEvent(true));
        }
        else{
            Log.d(TAG, "Offline now");
            EventBus.getDefault().postSticky(new NetworkChangeEvent(false));
        }
    }
}