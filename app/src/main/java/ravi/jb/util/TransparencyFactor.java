package ravi.jb.util;

/**
 * Created by brakisho on 1/27/2018.
 */

public enum TransparencyFactor {
    T100("FF"),
    T95("F2"),
    T90("E6"),
    T85("D9"),
    T80("CC"),
    T75("BF"),
    T70("B3"),
    T65("A6"),
    T60("99"),
    T55("8C"),
    T50("80"),
    T45("73"),
    T40("66"),
    T35("59"),
    T30("4D"),
    T25("40"),
    T20("33"),
    T15("26"),
    T10("1A"),
    T5("0D"),
    T0("00");
    private String value;
    private TransparencyFactor(String value){
        this.value=value;
    }

    public String getFactorValue(){
        return this.value;
    }
}
