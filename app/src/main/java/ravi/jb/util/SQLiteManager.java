package ravi.jb.util;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.AsyncTask;
import android.util.Log;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import ravi.jb.events.model.ConfigElement;
import ravi.jb.events.model.Event;
import ravi.jb.events.model.NewsElement;
import ravi.jb.events.model.SyllabusObj;

public class SQLiteManager extends SQLiteOpenHelper {

    private static final String TAG=SQLiteManager.class.getSimpleName();
    private Context context;
    public static final String DATABASE_NAME = "jbcollege.db";
    public static final String EVENTS_ARCHIVE = "events_archive";
    public static final String NEWS_ARCHIVE = "news_archive";
    public static final String SYLLABAI_ARCHIVE = "syllabi_archive";
    public static final String COLUMN_AUTOID = "id";
    public static final String COLUMN_EID = "eid";
    public static final String COLUMN_MSG = "msg";

    public static final String CONFIG_ARCHIVE = "config_archive";
    public static final String CONFIG_KEY_COLUMN = "key";
    public static final String CONFIG_VALUE_COLUMN = "value";





    public SQLiteManager(Context context)
    {
        super(context, DATABASE_NAME, null, 5);
        this.context=context;
    }

    @Override
    public  void onCreate(SQLiteDatabase db) {
        // TODO Auto-generated method stub
        db.execSQL(
                "create table " + EVENTS_ARCHIVE + " (" + COLUMN_AUTOID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                        + COLUMN_EID + " text,"
                        + COLUMN_MSG + " text)");

        db.execSQL(
                "create table " + NEWS_ARCHIVE + " (" + COLUMN_AUTOID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                        + COLUMN_EID + " text,"
                        + COLUMN_MSG + " text)");

        db.execSQL(
                "create table " + SYLLABAI_ARCHIVE + " (" + COLUMN_AUTOID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                        + COLUMN_EID + " text,"
                        + COLUMN_MSG + " text)");


        db.execSQL(
                "create table " + CONFIG_ARCHIVE + " (" + CONFIG_KEY_COLUMN + " text PRIMARY KEY,"
                        + CONFIG_VALUE_COLUMN + " text)");

    }

    @Override
    public  void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // TODO Auto-generated method stub
        db.execSQL("DROP TABLE IF EXISTS " + EVENTS_ARCHIVE);
        db.execSQL("DROP TABLE IF EXISTS " + NEWS_ARCHIVE);
        db.execSQL("DROP TABLE IF EXISTS " + SYLLABAI_ARCHIVE);
        db.execSQL("DROP TABLE IF EXISTS " + CONFIG_ARCHIVE);
        onCreate(db);
    }

    public   boolean deleteDatabase(){
        return context.deleteDatabase(DATABASE_NAME);
    }

    public  boolean updateEventArchive(Event event)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        Gson gson=new Gson();
        contentValues.put(COLUMN_MSG, gson.toJson(event));
        db.update(EVENTS_ARCHIVE, contentValues, COLUMN_EID + "='" + event.getId() + "';", null);

        return true;
    }

    public  boolean updateNewsArchive(NewsElement event)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        Gson gson=new Gson();
        contentValues.put(COLUMN_MSG, gson.toJson(event));
        db.update(NEWS_ARCHIVE, contentValues, COLUMN_EID + "='" + event.getId() + "';", null);

        return true;
    }

    public  boolean updateSyllabusArchive(SyllabusObj event)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        Gson gson=new Gson();
        contentValues.put(COLUMN_MSG, gson.toJson(event));
        db.update(SYLLABAI_ARCHIVE, contentValues, COLUMN_EID + "='" + event.getId() + "';", null);

        return true;
    }

    public List<Event> getAllEventsFromArchive(){
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res =  db.rawQuery( "select "+COLUMN_MSG+" from "+EVENTS_ARCHIVE+" order by "+COLUMN_AUTOID+" ASC;", null );
        List<Event> msgs=new ArrayList<>();
        if(res!=null){
            res.moveToFirst();
            Gson gson=new Gson();

            try {
                while(res.isAfterLast()==false){
                    int i=res.getColumnIndex(COLUMN_MSG);
                    msgs.add(gson.fromJson(res.getString(i), Event.class));
                    res.moveToNext();
                }
            }catch (Exception e){
                Log.e(TAG,e.getMessage());
            }
            res.close();
        }
        return msgs;
    }

    public List<NewsElement> getAllNewsFromArchive(){
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res =  db.rawQuery( "select "+COLUMN_MSG+" from "+NEWS_ARCHIVE+" order by "+COLUMN_AUTOID+" ASC;", null );
        List<NewsElement> msgs=new ArrayList<>();
        if(res!=null){
            res.moveToFirst();
            Gson gson=new Gson();

            try {
                while(res.isAfterLast()==false){
                    int i=res.getColumnIndex(COLUMN_MSG);
                    msgs.add(gson.fromJson(res.getString(i), NewsElement.class));
                    res.moveToNext();
                }
            }catch (Exception e){
                Log.e(TAG,e.getMessage());
            }
            res.close();
        }
        return msgs;
    }

    public List<SyllabusObj> getAllSyllabiFromArchive(){

        Log.d(TAG,"getAllSyllabiFromArchive:");
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res =  db.rawQuery( "select "+COLUMN_MSG+" from "+SYLLABAI_ARCHIVE+" order by "+COLUMN_AUTOID+" ASC;", null );
        List<SyllabusObj> msgs=new ArrayList<>();
        if(res!=null){
            res.moveToFirst();
            Gson gson=new Gson();

            try {
                while(res.isAfterLast()==false){
                    int i=res.getColumnIndex(COLUMN_MSG);
                    msgs.add(gson.fromJson(res.getString(i), SyllabusObj.class));
                    res.moveToNext();
                }
            }catch (Exception e){
                Log.e(TAG,e.getMessage());
            }
            res.close();
        }

        Log.d(TAG,msgs.toString());
        return msgs;
    }


    public  boolean insertIntoEventArchive(Event event)
    {
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            ContentValues contentValues = new ContentValues();
            Gson gson=new Gson();
            contentValues.put(COLUMN_EID, event.getId());
            contentValues.put(COLUMN_MSG, gson.toJson(event));
            long l= db.insert(EVENTS_ARCHIVE, null, contentValues);
            if(l>0)
                return true;

        }catch (Exception e){
            Log.e(TAG,e.getMessage());
        }

        return false;
    }

    public  boolean insertIntoNewsArchive(NewsElement event)
    {
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            ContentValues contentValues = new ContentValues();
            Gson gson=new Gson();
            contentValues.put(COLUMN_EID, event.getId());
            contentValues.put(COLUMN_MSG, gson.toJson(event));
            long l= db.insert(NEWS_ARCHIVE, null, contentValues);
            if(l>0)
                return true;

        }catch (Exception e){
            Log.e(TAG,e.getMessage());
        }

        return false;
    }

    public  boolean saveOrUpdateConfig(ConfigElement configElement)
    {
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            ContentValues contentValues = new ContentValues();
            contentValues.put(CONFIG_KEY_COLUMN, configElement.getKey());
            contentValues.put(CONFIG_VALUE_COLUMN, configElement.getValue());
            long l= db.insertWithOnConflict(CONFIG_ARCHIVE, null, contentValues,SQLiteDatabase.CONFLICT_IGNORE);
            if(l==-1)
                l=db.update(CONFIG_ARCHIVE, contentValues, "key=?", new String[] {configElement.getKey()});

            if(l>0)
                return true;

        }catch (Exception e){
            Log.e(TAG,e.getMessage());
        }

        return false;
    }


    public List<ConfigElement> getConfigs(){

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res =  db.rawQuery( "select "+CONFIG_KEY_COLUMN+","+CONFIG_VALUE_COLUMN+" from "+CONFIG_ARCHIVE, null );
        List<ConfigElement> configs=new ArrayList<>();
        if(res!=null){
            res.moveToFirst();
            try {
                while(res.isAfterLast()==false){

                        int i=res.getColumnIndex(CONFIG_KEY_COLUMN);
                        ConfigElement element=new ConfigElement();
                        element.setKey(res.getString(i));

                        i=res.getColumnIndex(CONFIG_VALUE_COLUMN);
                        element.setValue(res.getString(i));
                        configs.add(element);

                        res.moveToNext();
                }
            }catch (Exception e){
                Log.e(TAG,e.getMessage());
            }
            res.close();
        }
        return configs;
    }


    public  boolean deleteConfig(String key)
    {
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            db.execSQL("DELETE FROM " + CONFIG_ARCHIVE + " WHERE " + CONFIG_KEY_COLUMN + "='"+key+"';");
            return true;
        }catch (Exception e){
            Log.e(TAG,e.getMessage());
        }

        return false;
    }



    public  boolean insertIntoSyllabusArchive(SyllabusObj event)
    {
        try {


            Log.d(TAG,"insert:"+event);
            deleteFromSyllabusArchive(event);
            SQLiteDatabase db = this.getWritableDatabase();
            ContentValues contentValues = new ContentValues();
            Gson gson=new Gson();
            contentValues.put(COLUMN_EID, event.getId());
            contentValues.put(COLUMN_MSG, gson.toJson(event));
            long l= db.insert(SYLLABAI_ARCHIVE, null, contentValues);
            if(l>0)
                return true;

        }catch (Exception e){
            Log.e(TAG,e.getMessage());
        }

        return false;
    }


    public  boolean deleteFromEventArchive(Event event)
    {
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            db.execSQL("DELETE FROM " + EVENTS_ARCHIVE + " WHERE " + COLUMN_EID + "='"+event.getId()+"';");
            return true;
        }catch (Exception e){
            Log.e(TAG,e.getMessage());
        }

        return false;
    }

    public  boolean deleteFromNewsArchive(NewsElement event)
    {
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            db.execSQL("DELETE FROM " + NEWS_ARCHIVE + " WHERE " + COLUMN_EID + "='"+event.getId()+"';");
            return true;
        }catch (Exception e){
            Log.e(TAG,e.getMessage());
        }

        return false;
    }

    public  boolean deleteFromSyllabusArchive(SyllabusObj event)
    {
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            db.execSQL("DELETE FROM " + SYLLABAI_ARCHIVE + " WHERE " + COLUMN_EID + "='"+event.getId()+"';");
            return true;
        }catch (Exception e){
            Log.e(TAG,e.getMessage());
        }

        return false;
    }
    public  boolean deleteAllFromEventArchive()
    {
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            db.execSQL("DELETE FROM " + EVENTS_ARCHIVE +";");
            return true;
        }catch (Exception e){
            Log.e(TAG,e.getMessage());
        }

        return false;
    }

    public  boolean deleteAllFromNewsArchive()
    {
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            db.execSQL("DELETE FROM " + NEWS_ARCHIVE +";");
            return true;
        }catch (Exception e){
            Log.e(TAG,e.getMessage());
        }

        return false;
    }

    public  boolean deleteAllFromSyllabiArchive()
    {
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            db.execSQL("DELETE FROM " + SYLLABAI_ARCHIVE +";");
            return true;
        }catch (Exception e){
            Log.e(TAG,e.getMessage());
        }

        return false;
    }

    public  boolean insertAllIntoEventArchive(List<Event> list)
    {
        try {
            if(list!=null && list.size()>0)
            {
                for(Event e:list)
                    insertIntoEventArchive(e);

            }
        }catch (Exception e){
            Log.e(TAG,e.getMessage());
        }
        return false;
    }

    public  boolean insertAllIntoNewsArchive(List<NewsElement> list)
    {
        try {
            if(list!=null && list.size()>0)
            {
                for(NewsElement e:list)
                    insertIntoNewsArchive(e);

            }
        }catch (Exception e){
            Log.e(TAG,e.getMessage());
        }
        return false;
    }

    public  boolean insertAllIntoSyllabusArchive(List<SyllabusObj> list)
    {

        try {
            if(list!=null && list.size()>0)
            {
                for(SyllabusObj e:list)
                    insertIntoSyllabusArchive(e);

            }
        }catch (Exception e){
            Log.e(TAG,e.getMessage());
        }
        return false;
    }


    public void insertAllIntoEventArchiveAsync(final List<Event> events){


        AsyncTask asyncTask=new AsyncTask() {
            @Override
            protected Object doInBackground(Object[] params) {
                return insertAllIntoEventArchive(events);
            }
        };

        asyncTask.execute();
    }


    public void insertAllIntoNewsArchiveAsync(final List<NewsElement> events){


        AsyncTask asyncTask=new AsyncTask() {
            @Override
            protected Object doInBackground(Object[] params) {
                return insertAllIntoNewsArchive(events);
            }
        };

        asyncTask.execute();
    }


    public void insertAllIntoSyllabiArchiveAsync(final List<SyllabusObj> events){


        AsyncTask asyncTask=new AsyncTask() {
            @Override
            protected Object doInBackground(Object[] params) {
                return insertAllIntoSyllabusArchive(events);
            }
        };

        asyncTask.execute();
    }

}