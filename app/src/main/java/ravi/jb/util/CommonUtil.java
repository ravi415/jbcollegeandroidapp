package ravi.jb.util;

import android.app.Activity;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Environment;
import android.widget.Toast;

import java.io.File;

import ravi.jb.MainActivity;
import ravi.jb.R;

/**
 * Created by Endrjeet on 4/15/2017.
 */
public class CommonUtil {


    public static File getExternalStorageFolder(){

        String extStorageDirectory = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).toString();
        File folder = new File(extStorageDirectory);
        if(!folder.exists())
            folder.mkdir();

        return folder;
    }

    public static String getFileName(String path){

        try{

            int i=path.lastIndexOf("/");
            String fileName=path.substring(i+1);
            return fileName;
        }catch(Exception e)
        {
            e.printStackTrace();
        }

        return null;
    }

    public static String getEncodedFileName(String fileName){
        //return Base64.encodeToString(fileName.getBytes(),Base64.DEFAULT).replaceAll("[/+=]","-");
        return fileName;
    }

    public static boolean isNetworkOnline(Context context) {
        boolean status=false;
        try{
            ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo netInfo = cm.getNetworkInfo(0);
            if (netInfo != null && netInfo.getState()== NetworkInfo.State.CONNECTED) {
                status= true;
            }else {
                netInfo = cm.getNetworkInfo(1);
                if(netInfo!=null && netInfo.getState()==NetworkInfo.State.CONNECTED)
                    status= true;
            }
        }catch(Exception e){
            e.printStackTrace();
            status=false;
        }

        if(!status)
        {
            if(context instanceof MainActivity) {
                MainActivity activity = (MainActivity) context;
                activity.showSnackbarMessage(activity.getResources().getString(R.string.usr_offline_msg),1000,false);
            }
        }
        return status;
    }



    public static void showOfflineMsg(final Context context){

        ((Activity)context).runOnUiThread(new Runnable() {
            @Override
            public void run() {

                Toast.makeText(context,"Check network connectivity",Toast.LENGTH_SHORT).show();

            }
        });
    }

    public static void showToastMessage(final Context context,final String msg){

        ((Activity)context).runOnUiThread(new Runnable() {
            @Override
            public void run() {

                Toast.makeText(context,msg,Toast.LENGTH_SHORT).show();

            }
        });
    }
}
