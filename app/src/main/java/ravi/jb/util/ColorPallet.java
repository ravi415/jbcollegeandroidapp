package ravi.jb.util;

/**
 * Created by brakisho on 1/28/2018.
 */

public enum ColorPallet {

    CYAN_200("80DEEA"),
    LIGHT_BLUE_200("81D4FA"),
    TEAL_200("80CBC4"),
    GREEN_200("A5D6A7"),
    LIGHT_GREEN_200("C5E1A5"),
    LIME_200("E6EE9C"),
    YELLOW_200("FFF59D"),
    AMBER_200("FFE082"),
    ORANGE_200("FFCC80"),
    DEEP_ORANGE_200("FFAB91"),
    BROWN_200("BCAAA4"),
    GREY_200("EEEEEE"),
    BLU_GREY_200("B0BEC5"),
    RED_200("EF9A9A"),
    PINK_200("F48FB1"),
    PURPLE_200("CE93D8"),
    DEEP_PURPLE_200("B39DDB"),
    INDIGO_200("9FA8DA"),
    BLUE_200("90CAF9");

    private String value;
    private ColorPallet(String value){
        this.value=value;
    }
    public String getValue(){
        return this.value;
    }
}
