package ravi.jb.util;

import android.app.Activity;
import android.os.AsyncTask;
import android.util.Log;

import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;

import ravi.jb.JbApplication;

/**
 * Created by brakisho on 3/5/2018.
 */

public class CheckerTask extends AsyncTask<Void,Void,Void>
{

    private static final String TAG=CheckerTask.class.getSimpleName();
    private String url;
    private List<String> gImgs;
    private Activity activity;

    CheckerTask(String url,List<String> gImgs,Activity activity){
        this.url=url;
        this.gImgs=gImgs;
        this.activity=activity;
    }
    protected void onPreExecute() {
    }
    protected Void doInBackground(Void... params) {
        try {
            HttpURLConnection con =
                    (HttpURLConnection) new URL(url).openConnection();
            con.setRequestMethod("HEAD");
            int code=con.getResponseCode();
            if(code == HttpURLConnection.HTTP_OK) {
                this.gImgs.add(url);

                if(JbApplication.imageAdapter!=null){
                    activity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            JbApplication.imageAdapter.notifyDataSetChanged();
                        }
                    });
                }
            }
        }
        catch (final Exception e) {
            Log.e(TAG,""+e.getMessage());
        }
        return null;
    }



    protected void onPostExecute(Void result) {
        // dismiss progress dialog and update ui
    }
}
