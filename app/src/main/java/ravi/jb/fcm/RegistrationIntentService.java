package ravi.jb.fcm;

import android.app.IntentService;
import android.content.Intent;
import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;

import ravi.jb.BuildConfig;

/**
 * Created by brakisho on 9/3/2017.
 */

public class RegistrationIntentService extends IntentService {

    private static final String TAG = "RegIntentService";


    public RegistrationIntentService() {
        super(TAG);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        String token = FirebaseInstanceId.getInstance().getToken();

        if(BuildConfig.DEBUG)
            Log.d(TAG, "FCM Registration Token: " + token);
    }
}