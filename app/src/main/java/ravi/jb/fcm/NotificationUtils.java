package ravi.jb.fcm;

import android.app.ActivityManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.support.v4.app.NotificationCompat;
import android.text.TextUtils;
import android.util.Log;

import com.google.gson.Gson;

import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import ravi.jb.JbApplication;
import ravi.jb.MainActivity;
import ravi.jb.R;
import ravi.jb.events.activities.ScrollingEventDetailsActivity;
import ravi.jb.events.model.Event;

/**
 * Created by braj.kishore on 12/21/2016.
 */
public class NotificationUtils {

    private static String TAG = NotificationUtils.class.getSimpleName();
    private static String GROUP_KEY_MSG="group_key_msg";
    private List<String> messages=new ArrayList<String>(1);

    // notification icon
    private final int icon = R.drawable.jb;
    private Bitmap largeIcon=null;
    private Context mContext;

    public NotificationUtils(Context mContext) {
        this.mContext = mContext;
        largeIcon= BitmapFactory.decodeResource(mContext.getResources(),
                R.drawable.logo);

    }

    public void showNotificationMessage(JSONObject json) {
        try {


            String name=json.getString("title");
         if (TextUtils.isEmpty(name))
                return;

            String body=json.getString("body");
            String notif=json.getString("notif_msg");
            Event event=(new Gson()).fromJson(body,Event.class);
            JbApplication.selectedEvent=event;

            Intent resultIntent = new Intent(mContext, ScrollingEventDetailsActivity.class);
            Intent backIntent = new Intent(mContext, MainActivity.class);
            backIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);

            final PendingIntent resultPendingIntent = PendingIntent.getActivities(
                   mContext, 0,
                    new Intent[]{backIntent, resultIntent}, PendingIntent.FLAG_CANCEL_CURRENT | PendingIntent.FLAG_ONE_SHOT);

            final Uri alarmSound =RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

            NotificationCompat.Builder mBuilder=
                    new NotificationCompat.Builder(mContext)
                            .setSmallIcon(icon)
                            .setContentTitle("JB College")
                            .setStyle(new NotificationCompat.BigTextStyle()
                                    .bigText(TextUtils.isEmpty(notif)?"":notif))
                            .setContentText(name)
                            .setAutoCancel(true)
                            .setColor(mContext.getResources().getColor(R.color.colorAccent))
                            .setLargeIcon(largeIcon)
                            .setContentIntent(resultPendingIntent)
                            .setSound(alarmSound);

                NotificationCompat.InboxStyle inboxStyle = new NotificationCompat.InboxStyle();

            Log.d(TAG, "pending messages " + messages);
            inboxStyle.addLine(TextUtils.isEmpty(notif)?"":notif);
            mBuilder.setStyle(inboxStyle);
            Notification notification = mBuilder.build();
                NotificationManager notificationManager = (NotificationManager) mContext.getSystemService(Context.NOTIFICATION_SERVICE);
                notificationManager.notify(Config.NOTIFICATION_ID, notification);
                playNotificationSound();

        }
        catch(Exception e){
            Log.e(TAG,e.toString());
            e.printStackTrace();
        }
    }

    // Playing notification sound
    public void playNotificationSound() {
        try {
            Uri alarmSound =RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            Ringtone r = RingtoneManager.getRingtone(mContext, alarmSound);
            r.play();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Method checks if the app is in background or not
     */
    public static boolean isAppIsInBackground(Context context) {
        boolean isInBackground = true;
        ActivityManager am = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.KITKAT_WATCH) {
            List<ActivityManager.RunningAppProcessInfo> runningProcesses = am.getRunningAppProcesses();
            for (ActivityManager.RunningAppProcessInfo processInfo : runningProcesses) {
                if (processInfo.importance == ActivityManager.RunningAppProcessInfo.IMPORTANCE_FOREGROUND) {
                    for (String activeProcess : processInfo.pkgList) {
                        if (activeProcess.equals(context.getPackageName())) {
                            isInBackground = false;
                        }
                    }
                }
            }
        } else {
            List<ActivityManager.RunningTaskInfo> taskInfo = am.getRunningTasks(1);
            ComponentName componentInfo = taskInfo.get(0).topActivity;
            if (componentInfo.getPackageName().equals(context.getPackageName())) {
                isInBackground = false;
            }
        }

        return isInBackground;
    }

    // Clears notification tray messages
    public static void clearNotifications(Context context) {
        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.cancelAll();
    }

    public static long getTimeMilliSec(String timeStamp) {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        try {
            Date date = format.parse(timeStamp);
            return date.getTime();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return 0;
    }
}
