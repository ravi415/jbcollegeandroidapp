package ravi.jb.fcm;

import android.support.annotation.NonNull;
import android.util.Log;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.storage.FileDownloadTask;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.io.File;

import ravi.jb.util.CommonUtil;

/**
 * Created by brakisho on 12/11/2017.
 */

public class FirebaseStorageHandler {

    private static final String TAG=FirebaseStorageHandler.class.getSimpleName();
    private static StorageReference mStorageRef;
    private static String BASE_STORAGE_PATH_NAME="syllabi";
    private  FileDownloadStatus downloadStatus;
    private FirebaseStorageHandler(FileDownloadStatus fileDownloadStatus){
            mStorageRef = FirebaseStorage.getInstance().getReference();
        this.downloadStatus=fileDownloadStatus;
    }
public static FirebaseStorageHandler instance(FileDownloadStatus fileDownloadStatus){
        return new FirebaseStorageHandler(fileDownloadStatus);
}

public static interface FileDownloadStatus{

    public void onSuccess(String localUrl);
    public void onError(String message);
}


public void downloadFile(final String fileName){

    Log.d(TAG,"downloadFile:"+fileName);

    File folder = CommonUtil.getExternalStorageFolder();
    final File localFile = new File(folder,CommonUtil.getEncodedFileName(fileName));

    if(!localFile.exists()) {
        try {
            StorageReference reference = mStorageRef.child(BASE_STORAGE_PATH_NAME + "/" + fileName);
            reference.getFile(localFile)
                    .addOnSuccessListener(new OnSuccessListener<FileDownloadTask.TaskSnapshot>() {
                        @Override
                        public void onSuccess(FileDownloadTask.TaskSnapshot taskSnapshot) {
                            // Successfully downloaded data to local file
                            // ...

                            if (downloadStatus != null)
                                downloadStatus.onSuccess(localFile.getAbsolutePath());
                            Log.i(TAG, "Downloaded file:" + localFile.getAbsolutePath() + ":" + fileName);
                        }

                    }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception exception) {
                    // Handle failed download
                    // ...
                    if (downloadStatus != null)
                        downloadStatus.onError(exception.getMessage());

                    exception.printStackTrace();
                }
            });
        } catch (Exception e) {

            if (downloadStatus != null)
                downloadStatus.onError(e.getMessage());
            e.printStackTrace();
        }
    }
    else{
        Log.d(TAG,"File:"+fileName+":already exists");
        if (downloadStatus != null)
            downloadStatus.onSuccess(localFile.getAbsolutePath());
    }
}
}
