package ravi.jb.actvities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import ravi.jb.R;

public class AboutActivity extends AppCompatActivity {



    private static final String TAG=AboutActivity.class.getSimpleName();
    private Button jbGroupInfo;
    private Activity contextActivity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);
        contextActivity=this;
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        jbGroupInfo=(Button)findViewById(R.id.jbGroupInfo);


        jbGroupInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent=new Intent(contextActivity, JBGroupInfoActivity.class);
                contextActivity.startActivity(intent);

                Toast.makeText(contextActivity, "Clicked on jbGroupInfo ", Toast.LENGTH_SHORT).show();
            }
        });

    }

    public void onBackPressed() {
        finish();
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // action with ID action_refresh was selected
            case android.R.id.home:
                finish();
            default:
                break;
        }

        return true;
    }


}
