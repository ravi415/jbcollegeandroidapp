package ravi.jb.actvities;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.google.gson.Gson;

import ravi.jb.JbApplication;
import ravi.jb.R;
import ravi.jb.events.model.MailInfo;

public class QueryActivity extends AppCompatActivity {

    private static final String TAG=QueryActivity.class.getSimpleName();
    private Activity contextActivity;
    private Button submitBtn;
    private TextInputLayout usrTxt;
    private TextInputLayout guestName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_query);
        contextActivity=this;
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        submitBtn=(Button)findViewById(R.id.submitBtn);
        submitBtn.setBackgroundColor(contextActivity.getResources().getColor(R.color.colorAccent));
        usrTxt=(TextInputLayout)findViewById(R.id.usrTxt);
        guestName=(TextInputLayout)findViewById(R.id.guestName);

        submitBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(TextUtils.isEmpty(guestName.getEditText().getText()))
                    Toast.makeText(contextActivity,"Your name missing",Toast.LENGTH_SHORT).show();
                else if(TextUtils.isEmpty(usrTxt.getEditText().getText()))
                    Toast.makeText(contextActivity,"Your query missing",Toast.LENGTH_SHORT).show();
                else{
                    openMailIntent();
                }
            }
        });

    }

    private void openMailIntent(){

        String mailInfoStr= JbApplication.configs.get("mail.info");
        MailInfo mailInfo=(new Gson()).fromJson(mailInfoStr,MailInfo.class);

        Intent emailIntent = new Intent(
                android.content.Intent.ACTION_SENDTO);

        StringBuilder sb=new StringBuilder();
        sb.append("mailto:").append(mailInfo.getToMails()).append("?cc=").append(mailInfo.getCcMails()).append("&bcc=").append(mailInfo.getBccMails());
        emailIntent.putExtra(android.content.Intent.EXTRA_SUBJECT,
                mailInfo.getDefSubject());
        emailIntent.putExtra(Intent.EXTRA_TEXT, Html.fromHtml(usrTxt.getEditText().getText().toString()));
        emailIntent.setType(mailInfo.getMimeType());
        emailIntent.setData(Uri.parse(sb.toString()));
        emailIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(Intent.createChooser(emailIntent,
                "Send Email: "));
    }


    public void onBackPressed() {
        finish();
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // action with ID action_refresh was selected
            case android.R.id.home:
                finish();
            default:
                break;
        }

        return true;
    }

}
