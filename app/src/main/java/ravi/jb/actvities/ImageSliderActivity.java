package ravi.jb.actvities;

import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.MenuItem;

import ravi.jb.R;
import ravi.jb.adapters.ImageSlidingAdapter;

public class ImageSliderActivity extends AppCompatActivity {

    private static final String TAG=ImageSliderActivity.class.getSimpleName();
    private String urls[];
    int current_pos=-1;
    private String category;

    private RecyclerView recycler_view;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image_slider);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        recycler_view=findViewById(R.id.recycler_view);


        urls=getIntent().getStringArrayExtra("IMAGE_URLS");

        current_pos=getIntent().getIntExtra("CIMAGE_POS",-1);
        category=getIntent().getStringExtra("CATEGORY");


        ActionBar actionBar=getSupportActionBar();
        if(!TextUtils.isEmpty(category))
            actionBar.setTitle(category);


        LinearLayoutManager horizontalLayoutManager = new LinearLayoutManager(ImageSliderActivity.this, LinearLayoutManager.HORIZONTAL, false);
        recycler_view.setLayoutManager(horizontalLayoutManager);
        ImageSlidingAdapter imageSlidingAdapter=new ImageSlidingAdapter(urls,this);
        recycler_view.setAdapter(imageSlidingAdapter);
        recycler_view.scrollToPosition(current_pos);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // action with ID action_refresh was selected
            case android.R.id.home:
                finish();
            default:
                break;
        }

        return true;
    }
}
