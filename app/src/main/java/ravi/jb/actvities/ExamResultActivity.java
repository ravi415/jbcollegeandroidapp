package ravi.jb.actvities;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.webkit.JavascriptInterface;
import android.webkit.JsResult;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.util.HashMap;
import java.util.Map;

import ravi.jb.R;

import static ravi.jb.JbApplication.configs;

public class ExamResultActivity extends AppCompatActivity {

    private static final String TAG = ExamResultActivity.class.getSimpleName();
    private boolean jsCalled = false;
    private String jsCode;
    private WebView mWebView;
    private ProgressDialog progress;
    private String rollNum = "";
    private String regNum = "";
    private ExamResultActivity examResultActivity;
    private static final long TIMEOUT=90000;//1 MIN


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_exam_result);
        examResultActivity=this;
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        rollNum=getIntent().getStringExtra("ROLL_NUM");
        regNum=getIntent().getStringExtra("REG_NUM");

        Log.d(TAG,"Roll:"+rollNum+"::regnum:"+regNum);
        this.progress = new ProgressDialog(this);
        this.progress.setMessage("Getting Result...");
        this.progress.setProgressStyle(0);
        this.progress.setIndeterminate(true);
        this.progress.setCancelable(false);
        this.progress.show();
        loadHtml(configs.get("mdu.result.uri"));
        callUrl(configs.get("mdu.result.uri"));
        getSupportActionBar().setTitle("Result");
        this.jsCode = configs.get("mdu.result.parser.js");
        final long startTime=System.currentTimeMillis();
        final Handler handler=new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {

                if(progress!=null && progress.isShowing()) {
                    long elapsedTime=(System.currentTimeMillis()-startTime)/1000;
                    progress.setMessage("Waited for "+elapsedTime+"sec");
                    Handler handler1=new Handler();
                    handler1.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            if(progress!=null && progress.isShowing())
                                progress.dismiss();
                                finish();
                        }
                    },TIMEOUT);
                }
            }
        },TIMEOUT/2);



    }


    private void loadHtml(final String url){

        Thread t=new Thread(new Runnable() {
            @Override
            public void run() {

            try{


                Connection.Response response=Jsoup.connect(url).method(Connection.Method.GET).execute();
                Document doc=response.parse();
                Element form=doc.getElementById("form1");
                //Get input parameters of the form.
                Elements inputElements = form.getElementsByTag("input");
                //Iterate parameters and print name and value.
                Map<String,String> paramMap=new HashMap<String,String>();

                for (Element inputElement : inputElements) {
                    String key = inputElement.attr("name");
                    String value = inputElement.attr("value");
                    paramMap.put(key,value);
                }

                paramMap.put("txtRollNo","1234");
                paramMap.put("txtRegistrationNo","09876");
                paramMap.put("ScriptManager1","uPnlInstType|cmdbtnProceed");
                paramMap.put("__ASYNCPOST","true");
                paramMap.put("__EVENTARGUMENT","");
                paramMap.put("__EVENTTARGET","");
                paramMap.put("cmdbtnProceed.x","39");
                paramMap.put("cmdbtnProceed.y","6");

                        Connection connection=Jsoup.connect(url);
                            connection.userAgent("Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.75 Safari/537.36")
                                .timeout(10 * 1000)
                                .method(Connection.Method.POST);

                            connection.data(paramMap);
                            connection.header("Content-Type","application/x-www-form-urlencoded; charset=UTF-8");
                            connection.header("Accept","text/plain");
                            connection.cookies(response.cookies());
                            connection.followRedirects(true);



                    Document d=connection.post();
                    for(Map.Entry<String,String> entry:paramMap.entrySet())
                        Log.d(TAG,"entry::"+entry+"\n");

                System.out.print(d);

            } catch (java.io.IOException e){
                e.printStackTrace();
            }
            }
        });

        t.start();
    }


    public void callUrl(String url) {
        this.mWebView = ((WebView) findViewById(R.id.mWebView));
        this.mWebView.getSettings().setJavaScriptEnabled(true);
        this.mWebView.addJavascriptInterface(new WebAppInterface(this), "Android");
        this.mWebView.setVisibility(View.INVISIBLE);
        try {
            this.mWebView.loadUrl(url);
            this.mWebView.setWebChromeClient(new WebChromeClient() {
                public boolean onJsAlert(WebView paramAnonymousWebView,
                                         String paramAnonymousString1,
                                         String paramAnonymousString2,
                                         JsResult result) {
                            progress.hide();

                    new AlertDialog.Builder(examResultActivity)
                            .setMessage(paramAnonymousString2)
                            .setPositiveButton(android.R.string.ok,
                                    new AlertDialog.OnClickListener()
                                    {
                                        public void onClick(DialogInterface dialog, int wicht)
                                        {
                                            finish();
                                        }
                                    }).setCancelable(false)
                            .create()
                            .show();
                    return true;

                    //return super.onJsAlert(paramAnonymousWebView, paramAnonymousString1, paramAnonymousString2, paramAnonymousJsResult);
                }
            });
            this.mWebView.setWebViewClient(new WebViewClient() {

                public void onPageFinished(WebView paramAnonymousWebView, String paramAnonymousString) {
                    Log.d(TAG, "onpage finsihed, url=" + paramAnonymousString);
                    if (!jsCalled) {

                        jsCalled=true;
                        paramAnonymousWebView=mWebView;
                        Log.d(TAG, "Before Calling js");
                        mWebView.loadUrl(jsCode);
                        Log.d(TAG, "Called js");
                        Log.d(TAG, "Hide progress");
                    }
                    super.onPageFinished(mWebView, paramAnonymousString);
                }

                public void onPageStarted(WebView paramAnonymousWebView, String paramAnonymousString, Bitmap paramAnonymousBitmap) {
                    super.onPageStarted(paramAnonymousWebView, paramAnonymousString, paramAnonymousBitmap);
                    Log.d(TAG, "onpage start, url=" + paramAnonymousString);
                }
            });
            Log.d(TAG, "above return 1");
            return;
        } catch (Exception paramString) {
            for (; ; ) {
                Log.e("RESULT: ", "exception");
            }
        }

    }

    public class WebAppInterface {
        Context mContext;

        WebAppInterface(Context paramContext) {
            this.mContext = paramContext;
        }

        @JavascriptInterface
        public void showView() {
            Log.d(TAG, "showview");
            runOnUiThread(new Runnable() {
                public void run() {
                    mWebView.setVisibility(View.VISIBLE);
                    progress.hide();
                }
            });
            Log.d(TAG, "showview2");
        }
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // action with ID action_refresh was selected
            case android.R.id.home:
                finish();
            default:
                break;
        }

        return true;
    }
}



