package ravi.jb.actvities;

import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import ravi.jb.R;
import ravi.jb.util.CommonUtil;

public class DetailImageActivity extends AppCompatActivity {

    private static final String TAG=DetailImageActivity.class.getSimpleName();
    private String urls[];
    int current_pos=-1;
    private GestureDetector gestureDetector;
    private ImageView imageView;
    private String category;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_image);
        gestureDetector = new GestureDetector(new SwipeGestureDetector());

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        urls=getIntent().getStringArrayExtra("IMAGE_URLS");

        current_pos=getIntent().getIntExtra("CIMAGE_POS",-1);
        imageView=(ImageView)findViewById(R.id.detailedImg);
        category=getIntent().getStringExtra("CATEGORY");


        ActionBar actionBar=getSupportActionBar();
        if(!TextUtils.isEmpty(category))
            actionBar.setTitle(category);

        setImageView(current_pos);

    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if (gestureDetector.onTouchEvent(event)) {
            return true;
        }
        return super.onTouchEvent(event);
    }

    private void onLeftSwipe() {
        // Do something


        if(current_pos<urls.length-1)
            setImageView(++current_pos);

        Log.d(TAG, "left event:" + current_pos);

    }

    private void onRightSwipe() {
        // Do something
        if(current_pos>0)
            setImageView(--current_pos);


        Log.d(TAG, "right event " + current_pos);

    }

    private void setImageView(final int pos){

        if(pos<0 ||urls==null || pos>=urls.length)
            return;

                try {

                    if(!TextUtils.isEmpty(urls[pos])) {
                        Picasso.Builder picassoBuilder = new Picasso.Builder(this);
                        Picasso picasso = picassoBuilder.build();
                        picasso.load(urls[pos])
                                .into(imageView, new com.squareup.picasso.Callback() {
                                    @Override
                                    public void onSuccess() {
                                        imageView.setVisibility(View.VISIBLE);
                                       // imageView.startAnimation(animSlide);
                                    }

                                    @Override
                                    public void onError() {
                                        imageView.setVisibility(View.GONE);
                                        if(CommonUtil.isNetworkOnline(DetailImageActivity.this))
                                            CommonUtil.showOfflineMsg(DetailImageActivity.this);
                                        else
                                            CommonUtil.showToastMessage(DetailImageActivity.this,"Error in data get!");
                                    }
                                });

                    }

                }catch(Exception e){
                }
         current_pos = pos;

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // action with ID action_refresh was selected
            case android.R.id.home:

                finish();
            default:
                break;
        }

        return true;
    }


    // Private class for gestures
    private class SwipeGestureDetector extends GestureDetector.SimpleOnGestureListener {
        // Swipe properties, you can change it to make the swipe
        // longer or shorter and speed
        private static final int SWIPE_MIN_DISTANCE = 120;
        private static final int SWIPE_MAX_OFF_PATH = 200;
        private static final int SWIPE_THRESHOLD_VELOCITY = 200;

        @Override
        public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
            try {
                float diffAbs = Math.abs(e1.getY() - e2.getY());
                float diff = e1.getX() - e2.getX();

                if (diffAbs > SWIPE_MAX_OFF_PATH) {
                    return false;
                }

                // Left swipe
                if (diff > SWIPE_MIN_DISTANCE && Math.abs(velocityX) > SWIPE_THRESHOLD_VELOCITY) {
                    DetailImageActivity.this.onLeftSwipe();

                    // Right swipe
                } else if (-diff > SWIPE_MIN_DISTANCE && Math.abs(velocityX) > SWIPE_THRESHOLD_VELOCITY) {
                    DetailImageActivity.this.onRightSwipe();
                }
            } catch (Exception e) {
                Log.e("YourActivity", "Error on gestures");
            }
            return false;
        }
    }
}
