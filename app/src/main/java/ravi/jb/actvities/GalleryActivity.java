package ravi.jb.actvities;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.GridLayoutAnimationController;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ProgressBar;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

import ravi.jb.JbApplication;
import ravi.jb.R;
import ravi.jb.adapters.ImageAdapter;
import ravi.jb.events.model.GalleryItem;
import ravi.jb.events.model.NetworkChangeEvent;
import ravi.jb.util.CommonUtil;

public class GalleryActivity extends AppCompatActivity {


    private static final String TAG=GalleryActivity.class.getSimpleName();
    private Activity contextActivity;
    private ProgressBar progressBar;
    private ImageAdapter imageAdapter;
    private String category;
    private List<String> imgUrls=new ArrayList<>(1);


    @Subscribe(threadMode = ThreadMode.MAIN,sticky = true)
    public void onMessageEvent(NetworkChangeEvent event)
    {

    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gallery);
        contextActivity=this;

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        category=getIntent().getStringExtra("CATEGORY");
        ActionBar actionBar=getSupportActionBar();
        if(!TextUtils.isEmpty(category))
            actionBar.setTitle(category);

        progressBar=(ProgressBar) findViewById(R.id.progressBar);
        GridView gridView= (GridView )findViewById(R.id.gridView);


        GalleryItem galleryItem=JbApplication.galleryItem;
        int maxImages=galleryItem.getMax();
        String baseUri=galleryItem.getBasePath();

        for(int i=0;i<maxImages;i++) {
            imgUrls.add(baseUri + "gimg_" + i + ".jpg");
        }
        Log.d(TAG,"urls:"+imgUrls);
        imageAdapter=new ImageAdapter(contextActivity, imgUrls,progressBar );
        gridView.setAdapter(imageAdapter);

        Animation animation = AnimationUtils.loadAnimation(contextActivity,R.anim.layout_wave_scale);
        GridLayoutAnimationController controller = new GridLayoutAnimationController(animation, .2f, .2f);
        gridView.setLayoutAnimation(controller);


        if(imgUrls.size()>0)
            progressBar.setVisibility(View.VISIBLE);
        else
            progressBar.setVisibility(View.INVISIBLE);
        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                Intent intent=new Intent(contextActivity,ImageSliderActivity.class);
                intent.putExtra("IMAGE_URLS",imgUrls.toArray(new String[imgUrls.size()]));
                intent.putExtra("CIMAGE_POS",position);
                intent.putExtra("CATEGORY",category);
                startActivity(intent);

                Log.d(TAG, "Clicked on item::" + position);
            }
        });

        JbApplication.imageAdapter=imageAdapter;

        if(!CommonUtil.isNetworkOnline(contextActivity)) {
            CommonUtil.showOfflineMsg(contextActivity);
            progressBar.setVisibility(View.GONE);
        }


    }

    public void onBackPressed() {
        finish();
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // action with ID action_refresh was selected
            case android.R.id.home:
                finish();
            default:
                break;
        }

        return true;
    }


}

