package ravi.jb.fragments;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import ravi.jb.R;
import ravi.jb.actvities.ExamResultActivity;
import ravi.jb.util.CommonUtil;


public class ExamRsltFragment extends Fragment {

    private static final String TAG=ExamRsltFragment.class.getSimpleName();
    private Activity contextActivity;
    private TextInputLayout rollNumWrapper;
    private TextInputLayout regNumWrapper;
    private String rollText="";
    private String regText="";
    private Button submit;

    public static ExamRsltFragment newInstance() {
        ExamRsltFragment fragment = new ExamRsltFragment();
        return fragment;
    }

    public ExamRsltFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        contextActivity=getActivity();

    }

    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View rootView=inflater.inflate(R.layout.fragment_exmresult, container, false);
        rollNumWrapper=(TextInputLayout) rootView.findViewById(R.id.rollNumWrapper);
        regNumWrapper=(TextInputLayout) rootView.findViewById(R.id.regNumWrapper);

        readPrefValues();
        rollNumWrapper.getEditText().setText(rollText);
        regNumWrapper.getEditText().setText(regText);

        submit=(Button) rootView.findViewById(R.id.submit);
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(validateRollNum()&&validateRegNum()){

                    rollNumWrapper.setErrorEnabled(false);
                    rollNumWrapper.setError("");

                    if(CommonUtil.isNetworkOnline(contextActivity)) {
                        Intent intent = new Intent(contextActivity, ExamResultActivity.class);
                        intent.putExtra("ROLL_NUM", rollText);
                        intent.putExtra("REG_NUM", regText);
                        setPrefValues();
                        contextActivity.startActivity(intent);
                    }

                }
            }
        });

        return rootView;
    }


    private void setPrefValues(){

        SharedPreferences sharedPref = contextActivity.getSharedPreferences(
                getString(R.string.saved_detail_key), Context.MODE_PRIVATE);

        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString("ROLL_NUM",rollText);
        editor.putString("REG_NUM",regText);
        editor.commit();
    }

    private void readPrefValues(){
        SharedPreferences sharedPref = contextActivity.getSharedPreferences(
                getString(R.string.saved_detail_key), Context.MODE_PRIVATE);
        rollText=sharedPref.getString("ROLL_NUM","");
        regText=sharedPref.getString("REG_NUM","");
    }

    private boolean validateRollNum(){

        rollText=rollNumWrapper.getEditText().getText()!=null?rollNumWrapper.getEditText().getText().toString():"";
        rollText=rollText.trim();
        if(!TextUtils.isEmpty(rollText)&& !rollText.startsWith("0"))
            return true;

        rollNumWrapper.setErrorEnabled(true);
        rollNumWrapper.setError(contextActivity.getResources().getString(R.string.invalid_roll_num));
        return false;
    }
    private boolean validateRegNum(){

        regText=regNumWrapper.getEditText().getText()!=null?regNumWrapper.getEditText().getText().toString():"";
        regText=regText.trim();
        if(!TextUtils.isEmpty(regText)&& !regText.startsWith("0"))
            return true;

        regNumWrapper.setErrorEnabled(true);
        regNumWrapper.setError(contextActivity.getResources().getString(R.string.invalid_reg_num));
        return false;


    }



    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }
}
