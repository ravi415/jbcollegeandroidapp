package ravi.jb.fragments;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.gson.Gson;

import ravi.jb.JbApplication;
import ravi.jb.R;
import ravi.jb.adapters.TeamAdapter;
import ravi.jb.events.model.DevTeam;


public class TeamInfoFragment extends Fragment {

    private static final String TAG=TeamInfoFragment.class.getSimpleName();
    private Activity contextActivity;
    private RecyclerView recyclerView;

    public static TeamInfoFragment newInstance() {
        TeamInfoFragment fragment = new TeamInfoFragment();
        return fragment;
    }

    public TeamInfoFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        contextActivity=getActivity();

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View rootView=inflater.inflate(R.layout.fragment_teaminfo, container, false);
        recyclerView=rootView.findViewById(R.id.recycler_view);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(contextActivity);

        recyclerView.setLayoutManager(mLayoutManager);

        recyclerView.setItemAnimator(new DefaultItemAnimator());

        try {
            DevTeam devTeam=(new Gson()).fromJson(JbApplication.configs.get("dev.team"),DevTeam.class);
            Log.d(TAG,"devTeam:"+devTeam);
            if(devTeam!=null) {
                TeamAdapter teamAdapter = new TeamAdapter(devTeam.getMembers(),contextActivity);
                recyclerView.setAdapter(teamAdapter);
            }
        }catch(Exception e){
            Log.d(TAG,""+e.getMessage());
        }
        return rootView;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }
}
