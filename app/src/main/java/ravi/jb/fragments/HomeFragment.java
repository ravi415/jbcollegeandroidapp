package ravi.jb.fragments;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.gson.Gson;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import ravi.jb.JbApplication;
import ravi.jb.R;
import ravi.jb.actvities.AboutActivity;
import ravi.jb.actvities.CourcesActivity;
import ravi.jb.actvities.QueryActivity;
import ravi.jb.adapters.HomeAdapter;
import ravi.jb.events.activities.EventsActivity;
import ravi.jb.events.activities.GalleryCatActivity;
import ravi.jb.events.activities.NewsActivity;
import ravi.jb.events.activities.ScrollingEventDetailsActivity;
import ravi.jb.events.activities.SyllabiActivity;
import ravi.jb.events.model.PriorityNotice;
import ravi.jb.models.HomeItem;



public class HomeFragment extends Fragment {

    private static final String TAG=HomeFragment.class.getSimpleName();
    private Activity contextActivity;
    private TextView impNotice;
    private List<HomeItem> homeItemList;
    private HomeAdapter homeAdapter;

    public static HomeFragment newInstance() {
        HomeFragment fragment = new HomeFragment();
        return fragment;
    }





    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(PriorityNotice event)
    {
        handlePriorityMessage();
    }

    public HomeFragment() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        contextActivity=getActivity();

        homeItemList=new ArrayList<>();

        Resources.Theme theme=getContext().getTheme();
        Resources resources=getContext().getResources();
        homeItemList.add(new HomeItem(resources.getString(R.string.about_item),
                1,resources.getDrawable(R.drawable.about),
                AboutActivity.class));
        homeItemList.add(new HomeItem(
                resources.getString(R.string.notice_item),2,resources.getDrawable(R.drawable.calendar),
                EventsActivity.class));
        homeItemList.add(new HomeItem(resources.getString(R.string.news_item),
                3,resources.getDrawable(R.drawable.news),
                NewsActivity.class));
        homeItemList.add(new HomeItem(resources.getString(R.string.curriculum_item),4,resources.getDrawable(R.drawable.syllabi),
                SyllabiActivity.class));
        homeItemList.add(new HomeItem(resources.getString(R.string.courses_item),5,resources.getDrawable(R.drawable.courses),
                CourcesActivity.class));
        homeItemList.add(new HomeItem(resources.getString(R.string.check_result_item),6,resources.getDrawable(R.drawable.reportcard),
                null));
        homeItemList.add(new HomeItem(resources.getString(R.string.query_for_us_item),7,resources.getDrawable(R.drawable.qmark30),
                QueryActivity.class));
        homeItemList.add(new HomeItem(resources.getString(R.string.gallery_item),8,resources.getDrawable(R.drawable.gallery), GalleryCatActivity.class));

        Collections.sort(homeItemList,new HomeItemComparator());
    }


    private static class HomeItemComparator implements Comparator<HomeItem>{

        @Override
        public int compare(HomeItem x, HomeItem y){
            if(x.getOrder()<y.getOrder()){
                return -1;
            }
            else if(x.getOrder()>y.getOrder()){
                return 1;
            }
            return 0;
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View rootView=inflater.inflate(R.layout.fragment_home, container, false);

        impNotice=(TextView)rootView.findViewById(R.id.impNotice);

        homeAdapter=new HomeAdapter(homeItemList,contextActivity);
        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(contextActivity,2);

        RecyclerView recyclerView=(RecyclerView)rootView.findViewById(R.id.homeView);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(homeAdapter);


        return rootView;
    }


    private void handlePriorityMessage(){

        contextActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                final PriorityNotice priorityNotice=(new Gson()).fromJson(JbApplication.configs.get("priority.message"),PriorityNotice.class);

                Log.d(TAG,"priorityNotice:"+priorityNotice);
                if(priorityNotice!=null&& priorityNotice.isShowable()){
                    impNotice.setText(priorityNotice.getMessage());
                    impNotice.setVisibility(View.VISIBLE);

                    if(priorityNotice.getEvent()!=null) {
                        impNotice.setClickable(true);
                        impNotice.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                JbApplication.selectedEvent = priorityNotice.getEvent();
                                Intent i = new Intent(contextActivity, ScrollingEventDetailsActivity.class);
                                contextActivity.startActivity(i);
                            }
                        });
                    }
                }
                else
                    impNotice.setVisibility(View.GONE);

            }
        });
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        EventBus.getDefault().register(this);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        EventBus.getDefault().unregister(this);
    }
}
