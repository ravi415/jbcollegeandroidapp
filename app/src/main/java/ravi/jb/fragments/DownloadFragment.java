package ravi.jb.fragments;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import ravi.jb.JbApplication;
import ravi.jb.R;
import ravi.jb.common.FileDownloaderAsync;
import ravi.jb.util.CommonUtil;

import static android.content.Context.MODE_PRIVATE;


public class DownloadFragment extends Fragment {

    //http://jbcollege.in/jbadmissionform.pdf

    private Button btnBrochures;
    private Button btnAicteForm;
    private Button btnAdmissionForm;
    private String aicteURL;
    private String aicteFileName;
    private String brochureURL;
    private String brochureFileName;
    private String admFormURL;
    private String admFileName;

    public String cUrl,cFileName;
    private static final int EXTERNAL_STORAGE_PERMISSION_CONSTANT = 100;
    private static final int REQUEST_PERMISSION_SETTING = 101;
    private boolean sentToSettings = false;
    private SharedPreferences permissionStatus;

    private Context context;
    public static DownloadFragment newInstance() {
        DownloadFragment fragment = new DownloadFragment();
        return fragment;

    }

    public DownloadFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context=getActivity();


        aicteURL= JbApplication.configs.get("aicte.file.uri");
        aicteFileName=CommonUtil.getFileName(aicteURL);
        brochureURL=JbApplication.configs.get("brochure.file.uri");
        brochureFileName=CommonUtil.getFileName(brochureURL);
        admFormURL=JbApplication.configs.get("admission.file.uri");
        admFileName=CommonUtil.getFileName(admFormURL);



    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView=inflater.inflate(R.layout.fragment_download, container, false);
        btnBrochures=(Button)rootView.findViewById(R.id.btnBrochures);
        btnAicteForm=(Button)rootView.findViewById(R.id.btnAicteForm);
        btnAdmissionForm=(Button)rootView.findViewById(R.id.btnAdmissionForm);


        btnBrochures.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(CommonUtil.isNetworkOnline(context))
                    checkExternalStoragePermission(brochureURL,brochureFileName);
                else
                    CommonUtil.showOfflineMsg(context);
            }
        });


        btnAicteForm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                    if(CommonUtil.isNetworkOnline(context))
                        checkExternalStoragePermission(aicteURL,aicteFileName);
                    else
                        CommonUtil.showOfflineMsg(context);
                }
        });

        btnAdmissionForm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            if(CommonUtil.isNetworkOnline(context))
                checkExternalStoragePermission(admFormURL,admFileName);
            else
                CommonUtil.showOfflineMsg(context);
            }
        });

        // Inflate the layout for this fragment
        return rootView;

    }


    private void checkExternalStoragePermission(String url,String fileName) {


        cUrl=url;
        cFileName=fileName;

        permissionStatus = getActivity().getSharedPreferences("permissionStatus",MODE_PRIVATE);

        if (ActivityCompat.checkSelfPermission(context, android.Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale((Activity) context, android.Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                //Show Information about why you need the permission
                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                builder.setTitle("Need Storage Permission");
                builder.setMessage("This app needs storage permission.");
                builder.setPositiveButton("Grant", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                        ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                                EXTERNAL_STORAGE_PERMISSION_CONSTANT);
                    }
                });
                builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
                builder.show();
            } else if (permissionStatus.getBoolean(Manifest.permission.WRITE_EXTERNAL_STORAGE, false)) {
                //Previously Permission Request was cancelled with 'Dont Ask Again',
                // Redirect to Settings after showing Information about why you need the permission
                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                builder.setTitle("Need Storage Permission");
                builder.setMessage("This app needs storage permission.");
                builder.setPositiveButton("Grant", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                        sentToSettings = true;
                        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                        Uri uri = Uri.fromParts("package", getActivity().getPackageName(), null);
                        intent.setData(uri);
                        startActivityForResult(intent, REQUEST_PERMISSION_SETTING);
                        Toast.makeText(getActivity().getBaseContext(), "Go to Permissions to Grant Storage", Toast.LENGTH_LONG).show();
                    }
                });
                builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
                builder.show();
            } else {
                //just request the permission
                ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, EXTERNAL_STORAGE_PERMISSION_CONSTANT);
            }

            SharedPreferences.Editor editor = permissionStatus.edit();
            editor.putBoolean(Manifest.permission.WRITE_EXTERNAL_STORAGE,true);
            editor.commit();

        } else {
            //You already have the permission, just go ahead.
            invokeAsync();
        }

    }


    public void invokeAsync(){
        FileDownloaderAsync.newInstance(context).execute(cUrl, cFileName);
    }


    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }
}
