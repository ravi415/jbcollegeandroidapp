package ravi.jb;

import android.app.Application;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.util.Log;

import com.google.firebase.crash.FirebaseCrash;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

import ravi.jb.adapters.ImageAdapter;
import ravi.jb.events.model.ConfigElement;
import ravi.jb.events.model.Event;
import ravi.jb.events.model.GalleryItem;
import ravi.jb.events.model.NewsElement;
import ravi.jb.events.model.PriorityNotice;
import ravi.jb.events.model.SyllabusObj;
import ravi.jb.util.CommonUtil;
import ravi.jb.util.NetworkChangeReceiver;
import ravi.jb.util.SQLiteManager;

/**
 * Created by brakisho on 8/3/2017.
 */

public class JbApplication extends Application {
    private static final String TAG=JbApplication.class.getSimpleName();

    public static List<String> gImgs=new ArrayList<>(1);
    public static Event selectedEvent;
    public static NewsElement selectedNews;

    public static GalleryItem galleryItem;
    public static SyllabusObj.Semester selectedSemester;
    public static ImageAdapter imageAdapter;
    public static SQLiteManager sqLiteManager;
    public static ConcurrentHashMap<String,String> configs=new ConcurrentHashMap<>();

    @Override
    public void onCreate()
    {
        super.onCreate();
        Log.d(TAG,"Application is started");
        if (!BuildConfig.ENABLE_CRASHLYTICS) {
            Thread.setDefaultUncaughtExceptionHandler(new Thread.UncaughtExceptionHandler() {
                @Override
                public void uncaughtException(Thread paramThread, Throwable paramThrowable) {
                    Log.wtf("Alert", paramThrowable.getMessage(), paramThrowable);
                    System.exit(2); //Prevents the service/app from freezing
                }
            });
        }

        FirebaseCrash.setCrashCollectionEnabled(BuildConfig.ENABLE_CRASHLYTICS);
        sqLiteManager=new SQLiteManager(this);

        //FirebaseStorageHandler.instance().downloadFile("ACA.html");
        registerForNetworkChange();
        (new Thread(new Runnable() {
            @Override
            public void run() {
                loadConfigs();
                Log.d(TAG,"Loaded configs:"+configs);
            }
        })).start();
    }


    private void registerForNetworkChange(){
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(ConnectivityManager.CONNECTIVITY_ACTION);
        registerReceiver(new NetworkChangeReceiver(), intentFilter);
    }


    private void loadConfigs() {

        if (CommonUtil.isNetworkOnline(getApplicationContext())) {

            FirebaseDatabase mFirebaseInstance = FirebaseDatabase.getInstance();
            Query query = mFirebaseInstance.getReference("configs").limitToFirst(100);
            query.addChildEventListener(childEventListener);
        }
        else{
            List<ConfigElement> offlineList=sqLiteManager.getConfigs();
            for(ConfigElement configElement:offlineList)
                configs.put(configElement.getKey(),configElement.getValue());
        }
        Log.d(TAG,"mFirebaseInstance:"+configs);
    }

    private void saveDataSnapshot(DataSnapshot dataSnapshot){
        if(dataSnapshot!=null) {

            try {

                ConfigElement configElement = dataSnapshot.getValue(ConfigElement.class);
                if (configElement != null) {

                    String key = configElement.getKey();
                    String val = configElement.getValue();
                    if (configElement.getValue().startsWith("'") && configElement.getValue().endsWith("'")) {
                        val = configElement.getValue().substring(1);
                        val = val.substring(0, val.length() - 1);
                        configElement.setValue(val);
                    }

                    Log.d(TAG, "config:" + configElement);
                    configs.put(key, val);
                    sqLiteManager.saveOrUpdateConfig(configElement);

                    if ("priority.message".equalsIgnoreCase(key))
                        EventBus.getDefault().post(new PriorityNotice());

                }
            }catch(Exception e){
                Log.d(TAG,""+e.getMessage());
            }
        }
    }

    private void removeDataSnapshot(DataSnapshot dataSnapshot){
        if(dataSnapshot!=null) {

            try {
                ConfigElement configElement = dataSnapshot.getValue(ConfigElement.class);
                if (configElement != null) {
                    configs.remove(configElement.getKey());
                    sqLiteManager.deleteConfig(configElement.getKey());

                    if ("priority.message".equalsIgnoreCase(configElement.getKey()))
                        EventBus.getDefault().post(new PriorityNotice());
                }
            }catch(Exception e)
            {

            }

        }
    }

    private ChildEventListener childEventListener=new ChildEventListener() {
        @Override
        public void onChildAdded(DataSnapshot dataSnapshot, String s) {
            saveDataSnapshot(dataSnapshot);
        }

        @Override
        public void onChildChanged(DataSnapshot dataSnapshot, String s) {
            saveDataSnapshot(dataSnapshot);
        }

        @Override
        public void onChildRemoved(DataSnapshot dataSnapshot) {
            removeDataSnapshot(dataSnapshot);
        }

        @Override
        public void onChildMoved(DataSnapshot dataSnapshot, String s) {

        }

        @Override
        public void onCancelled(DatabaseError databaseError) {
        }
    };

}
