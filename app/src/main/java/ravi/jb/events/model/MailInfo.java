package ravi.jb.events.model;

/**
 * Created by brakisho on 2/18/2018.
 */

public class MailInfo {

    private String toMails;
    private String ccMails;
    private String bccMails;
    private String defSubject="Query";
    private String mimeType="message/rfc822";

    public String getToMails() {
        return toMails;
    }

    public void setToMails(String toMails) {
        this.toMails = toMails;
    }

    public String getCcMails() {
        return ccMails;
    }

    public void setCcMails(String ccMails) {
        this.ccMails = ccMails;
    }

    public String getBccMails() {
        return bccMails;
    }

    public void setBccMails(String bccMails) {
        this.bccMails = bccMails;
    }

    public String getDefSubject() {
        return defSubject;
    }

    public void setDefSubject(String defSubject) {
        this.defSubject = defSubject;
    }

    public String getMimeType() {
        return mimeType;
    }

    public void setMimeType(String mimeType) {
        this.mimeType = mimeType;
    }
}

