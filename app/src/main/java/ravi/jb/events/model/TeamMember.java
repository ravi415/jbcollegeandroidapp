package ravi.jb.events.model;

import java.util.List;

/**
 * Created by brakisho on 3/5/2018.
 */

public class TeamMember {

    private String name;
    private String about;
    private String imgUrl;
    private List<String> emails;
    private List<String> personalLinks;

    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAbout() {
        return about;
    }

    public void setAbout(String about) {
        this.about = about;
    }

    public List<String> getEmails() {
        return emails;
    }

    public void setEmails(List<String> emails) {
        this.emails = emails;
    }

    public List<String> getPersonalLinks() {
        return personalLinks;
    }

    public void setPersonalLinks(List<String> personalLinks) {
        this.personalLinks = personalLinks;
    }

    @Override
    public String toString() {
        return "TeamMember{" +
                "name='" + name + '\'' +
                ", about='" + about + '\'' +
                ", imgUrl='" + imgUrl + '\'' +
                ", emails=" + emails +
                ", personalLinks=" + personalLinks +
                '}';
    }
}
