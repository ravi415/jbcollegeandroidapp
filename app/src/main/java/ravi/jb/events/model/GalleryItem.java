package ravi.jb.events.model;

/**
 * Created by brakisho on 3/3/2018.
 */

public class GalleryItem {

    private String category;
    private String catImgName;
    private String basePath;
    private int max;

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getCatImgName() {
        return catImgName;
    }

    public void setCatImgName(String catImgName) {
        this.catImgName = catImgName;
    }

    public String getBasePath() {
        return basePath;
    }

    public void setBasePath(String basePath) {
        this.basePath = basePath;
    }

    public int getMax() {
        return max;
    }

    public void setMax(int max) {
        this.max = max;
    }


    @Override
    public String toString() {
        return "GalleryItem{" +
                "category='" + category + '\'' +
                ", catImgName='" + catImgName + '\'' +
                ", basePath='" + basePath + '\'' +
                ", max=" + max +
                '}';
    }
}
