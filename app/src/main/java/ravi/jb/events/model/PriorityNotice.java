package ravi.jb.events.model;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by brakisho on 2/18/2018.
 */

public class PriorityNotice {

    private String message;
    private String startTime;
    private String endTime;
    private Event event;

    public Event getEvent() {
        return event;
    }

    public void setEvent(Event event) {
        this.event = event;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    @Override
    public String toString() {
        return "PriorityNotice{" +
                "message='" + message + '\'' +
                ", startTime='" + startTime + '\'' +
                ", endTime='" + endTime + '\'' +
                '}';
    }

    public boolean isShowable(){

        try {

            SimpleDateFormat format=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date date1=format.parse(startTime);
            Date date2=format.parse(endTime);

            Date curDate=new Date();
            if(curDate.before(date2)&&curDate.after(date1))
                return true;

        }catch(Exception e)
        {

        }

        return false;
    }

}
