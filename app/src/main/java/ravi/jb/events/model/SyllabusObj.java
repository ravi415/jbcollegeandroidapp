package ravi.jb.events.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class SyllabusObj {

    @Expose
    private String courceName;

    @SerializedName("id")
    @Expose
    private String id="";

    @SerializedName("semesters")
    @Expose
    private List<Semester> semesters = null;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCourceName() {
        return courceName;
    }

    public void setCourceName(String courceName) {
        this.courceName = courceName;
    }

    public List<Semester> getSemesters() {
        return semesters;
    }

    public void setSemesters(List<Semester> semesters) {
        this.semesters = semesters;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SyllabusObj that = (SyllabusObj) o;

        return id.equals(that.id);

    }

    @Override
    public int hashCode() {
        return id.hashCode();
    }

    public static class Semester implements Parcelable {

    @SerializedName("semesterName")
    @Expose
    private String semesterName;

    @SerializedName("path")
    @Expose
    private String path = null;

    public String getSemesterName() {
        return semesterName;
    }

    public void setSemesterName(String semesterName) {
        this.semesterName = semesterName;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    @Override
    public String toString() {
        return semesterName;
    }

    public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
        public Semester createFromParcel(Parcel in) {
            return new Semester(in);
        }

        public Semester[] newArray(int size) {
            return new Semester[size];
        }
    };

    public Semester(){}
    // Parcelling part
    public Semester(Parcel in){
        this.semesterName = in.readString();
        this.path= in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.semesterName);
        dest.writeString(this.path);
    }

}
}
