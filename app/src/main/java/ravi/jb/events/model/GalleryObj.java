package ravi.jb.events.model;

import java.util.List;

/**
 * Created by brakisho on 3/3/2018.
 */

public class GalleryObj {

    private List<GalleryItem> galleryItems;

    public List<GalleryItem> getGalleryItems() {
        return galleryItems;
    }

    public void setGalleryItems(List<GalleryItem> galleryItems) {
        this.galleryItems = galleryItems;
    }
}
