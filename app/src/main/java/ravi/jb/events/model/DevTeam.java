package ravi.jb.events.model;

import java.util.List;

/**
 * Created by brakisho on 3/5/2018.
 */

public class DevTeam {

    private List<TeamMember> members;

    public List<TeamMember> getMembers() {
        return members;
    }

    public void setMembers(List<TeamMember> members) {
        this.members = members;
    }

    @Override
    public String toString() {
        return "DevTeam{" +
                "members=" + members +
                '}';
    }
}
