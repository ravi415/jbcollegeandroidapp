package ravi.jb.events.model;

/**
 * Created by brakisho on 12/13/2017.
 */

public class NetworkChangeEvent {

    private boolean isOnline;
    public NetworkChangeEvent(boolean isOnline){
        this.isOnline=isOnline;
    }

    public boolean isOnline() {
        return isOnline;
    }
}
