package ravi.jb.events.activities;

import android.Manifest;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.ListIterator;

import ravi.jb.JbApplication;
import ravi.jb.R;
import ravi.jb.events.adapters.RecyclerSyllabusAdapter;
import ravi.jb.events.model.NetworkChangeEvent;
import ravi.jb.events.model.SyllabusObj;
import ravi.jb.util.CommonUtil;
import ravi.jb.util.SQLiteManager;

public class SyllabiActivity extends AppCompatActivity {
private static final String TAG=SyllabiActivity.class.getSimpleName();

    private FirebaseDatabase mFirebaseInstance;
    private RecyclerSyllabusAdapter adapter;
    private List<SyllabusObj> syllabusObjs=new ArrayList<>(1);
    private int pageSize=50;
    private SQLiteManager sqLiteManager;

    public View progressBar;
    private View loadIndicatorLayout;
    public TextView loadMessage;
    private boolean isOffineData=true;
    private static final int EXTERNAL_STORAGE_PERMISSION_CONSTANT2 = 999;
    private static final int REQUEST_PERMISSION_SETTING2 = 9999;
    private SharedPreferences permissionStatus;




    private Activity contextActivity;


    @Subscribe(threadMode = ThreadMode.MAIN,sticky = true)
    public void onMessageEvent(NetworkChangeEvent event)
    {
        if(event.isOnline()) {
            isOffineData=false;
            loadData();
        }
        else {
            isOffineData=true;
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_syllabi);
        contextActivity=this;
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        sqLiteManager= JbApplication.sqLiteManager;


        progressBar=findViewById(R.id.progressBar);
        loadMessage=(TextView) findViewById(R.id.loadMessage);
        loadIndicatorLayout=findViewById(R.id.loadIndicatorLayout);

        if(JbApplication.configs.get("max.firedb.fetch.count")!=null)
            pageSize=Integer.parseInt(JbApplication.configs.get("max.firedb.fetch.count"));

        setUp();
        checkExternalStoragePermission();
    }


    private void openDialog(){


        loadIndicatorLayout.setVisibility(View.VISIBLE);
        progressBar.setVisibility(View.VISIBLE);
    }

    private void setUp(){

        adapter=new RecyclerSyllabusAdapter(syllabusObjs,contextActivity);
        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(contextActivity,2);

        RecyclerView recyclerView=(RecyclerView)findViewById(R.id.recycler_view);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(adapter);


        isOffineData=!CommonUtil.isNetworkOnline(contextActivity);
        Log.d(TAG,"isOffineData:"+isOffineData);
        openDialog();

        if(!isOffineData) {

            loadData();
        }
        else {
            if(syllabusObjs==null)
                syllabusObjs=new ArrayList<>();

            syllabusObjs.clear();
            syllabusObjs.addAll(sqLiteManager.getAllSyllabiFromArchive());

        }

        notifyUI();


        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

            }

            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }
        });

    }

    private void loadData(){

        try {

            if(syllabusObjs!=null)
                syllabusObjs.clear();
            else
                syllabusObjs=new ArrayList<>();

            //sqLiteManager.deleteAllFromSyllabiArchive();

            mFirebaseInstance = FirebaseDatabase.getInstance();
            Query query = mFirebaseInstance.getReference("syllabi").limitToFirst(pageSize);
            query.addChildEventListener(childEventListener);
        }catch (Exception e){
            Log.d(TAG,""+e.getMessage());
        }
    }

    private ChildEventListener childEventListener=new ChildEventListener() {
        @Override
        public void onChildAdded(DataSnapshot dataSnapshot, String s) {

            if(dataSnapshot!=null) {
                SyllabusObj syllabusObj = dataSnapshot.getValue(SyllabusObj.class);
                if (syllabusObj != null) {

                    if(syllabusObjs.contains(syllabusObj)) {
                        syllabusObjs.remove(syllabusObj);
                        deleteFilesIfExist(syllabusObj);
                    }

                    syllabusObjs.add(0, syllabusObj);
                    sqLiteManager.insertIntoSyllabusArchive(syllabusObj);
                    notifyUI();
                }
            }
        }

        @Override
        public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            if(dataSnapshot!=null) {
                SyllabusObj syllabusObj = dataSnapshot.getValue(SyllabusObj.class);
                if (syllabusObj != null) {

                    int i = syllabusObjs.indexOf(syllabusObj);
                    if (i >= 0) {
                        syllabusObjs.set(i, syllabusObj);
                        sqLiteManager.updateSyllabusArchive(syllabusObj);
                        notifyUI();
                        deleteFilesIfExist(syllabusObj);
                    }
                }
            }
        }

        @Override
        public void onChildRemoved(DataSnapshot dataSnapshot) {

            if(dataSnapshot!=null) {
                SyllabusObj syllabusObj = dataSnapshot.getValue(SyllabusObj.class);
                if (syllabusObj != null) {
                    syllabusObjs.remove(syllabusObj);
                    sqLiteManager.deleteFromSyllabusArchive(syllabusObj);
                    notifyUI();
                    deleteFilesIfExist(syllabusObj);
                }
            }
        }

        @Override
        public void onChildMoved(DataSnapshot dataSnapshot, String s) {

        }

        @Override
        public void onCancelled(DatabaseError databaseError) {
            dismissProgressDialog();
            contextActivity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(contextActivity,"Error in connecting server.",Toast.LENGTH_SHORT).show();
                }
            });
        }
    };

    private void dismissProgressDialog(){
        progressBar.setVisibility(View.INVISIBLE);
        loadIndicatorLayout.setVisibility(View.INVISIBLE);
    }

    private void notifyUI(){


        try {
            ListIterator<SyllabusObj> itr = syllabusObjs.listIterator();
            while (itr.hasNext())
                if (itr.next().getId()==null||!itr.next().getId().matches("\\d+"))
                    itr.remove();


            Collections.sort(syllabusObjs, new Comparator<SyllabusObj>() {
                @Override
                public int compare(SyllabusObj o1, SyllabusObj o2) {
                    return o1.getId().compareTo(o2.getId());
                }
            });

        }catch (Exception e){

        }
        contextActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                adapter.notifyDataSetChanged();
                dismissProgressDialog();
            }
        });
    }


    private void deleteFilesIfExist(final SyllabusObj obj){


        AsyncTask task=new AsyncTask() {
            @Override
            protected Object doInBackground(Object[] params) {

                try {
                    for(SyllabusObj.Semester semester:obj.getSemesters()) {
                        File file = new File(CommonUtil.getExternalStorageFolder() + File.pathSeparator + CommonUtil.getEncodedFileName(semester.getPath()));
                        if(file.exists())
                            file.delete();
                    }
                }catch(Exception e){
                    Log.d(TAG,""+e.getMessage());
                }
                return null;
            }
        };

        task.execute();
    }


    public void onBackPressed() {
        finish();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.refreshable_menu, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // action with ID action_refresh was selected
            case android.R.id.home:
                finish();
            case R.id.action_refresh:

                setUp();
                break;
            default:
                break;
        }

        return true;
    }



    private void checkExternalStoragePermission() {

        permissionStatus = getSharedPreferences("permissionStatus",MODE_PRIVATE);

        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                //Show Information about why you need the permission
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setTitle("Need Storage Permission");
                builder.setMessage("This app needs storage permission.");
                builder.setPositiveButton("Grant", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                        ActivityCompat.requestPermissions(contextActivity, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                                EXTERNAL_STORAGE_PERMISSION_CONSTANT2);
                    }
                });
                builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
                builder.show();
            } else if (permissionStatus.getBoolean(Manifest.permission.WRITE_EXTERNAL_STORAGE, false)) {
                //Previously Permission Request was cancelled with 'Dont Ask Again',
                // Redirect to Settings after showing Information about why you need the permission
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setTitle("Need Storage Permission");
                builder.setMessage("This app needs storage permission.");
                builder.setPositiveButton("Grant", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                        Uri uri = Uri.fromParts("package", contextActivity.getPackageName(), null);
                        intent.setData(uri);
                        startActivityForResult(intent, REQUEST_PERMISSION_SETTING2);
                        Toast.makeText(contextActivity, "Go to Permissions to Grant Storage", Toast.LENGTH_LONG).show();
                    }
                });
                builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
                builder.show();
            } else {
                //just request the permission
                ActivityCompat.requestPermissions(contextActivity, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, EXTERNAL_STORAGE_PERMISSION_CONSTANT2);
            }

            SharedPreferences.Editor editor = permissionStatus.edit();
            editor.putBoolean(Manifest.permission.WRITE_EXTERNAL_STORAGE,true);
            editor.commit();

        } else {
            //You already have the permission, just go ahead.

            Log.d(TAG,"checkExternalStoragePermission:Already has permission");
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        Log.d(TAG,"onRequestPermissionsResult:"+requestCode);
        if (requestCode == EXTERNAL_STORAGE_PERMISSION_CONSTANT2) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                //The External Storage Write Permission is granted to you... Continue your left job...
                setUp();
            } else {
                if (ActivityCompat.shouldShowRequestPermissionRationale(contextActivity, android.Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                    //Show Information about why you need the permission
                    AlertDialog.Builder builder = new AlertDialog.Builder(contextActivity);
                    builder.setTitle("Need Storage Permission");
                    builder.setMessage("This app needs storage permission");
                    builder.setPositiveButton("Grant", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();


                            ActivityCompat.requestPermissions(contextActivity, new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, EXTERNAL_STORAGE_PERMISSION_CONSTANT2);


                        }
                    });
                    builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();
                        }
                    });
                    builder.show();
                } else {
                    Toast.makeText(getBaseContext(),"Unable to get Permission",Toast.LENGTH_LONG).show();
                }
            }
        }
    }


}
