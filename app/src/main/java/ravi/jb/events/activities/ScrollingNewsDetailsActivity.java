package ravi.jb.events.activities;

import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import ravi.jb.JbApplication;
import ravi.jb.R;
import ravi.jb.events.model.NewsElement;
import ravi.jb.util.CommonUtil;

public class ScrollingNewsDetailsActivity extends AppCompatActivity {

    private static final String TAG=ScrollingNewsDetailsActivity.class.getSimpleName();
    private NewsElement event;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scrolling_news_details);

        event= JbApplication.selectedNews;

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(event.getName());

        Log.d(TAG,"Selected Event is:" +event);

        final ImageView eventBigImg=(ImageView)findViewById(R.id.eventBigImg);


        if(event.getImgUrlBig()!=null && !TextUtils.isEmpty(event.getImgUrlBig())) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Picasso picasso = Picasso.with(ScrollingNewsDetailsActivity.this);
                    picasso.load(event.getImgUrlBig())
                            .error(R.drawable.desk)
                            .into(eventBigImg, new com.squareup.picasso.Callback(){
                                @Override
                                public void onSuccess() {
                                }

                                @Override
                                public void onError() {
                                    if(CommonUtil.isNetworkOnline(ScrollingNewsDetailsActivity.this))
                                        CommonUtil.showOfflineMsg(ScrollingNewsDetailsActivity.this);
                                    else
                                        CommonUtil.showToastMessage(ScrollingNewsDetailsActivity.this,"You are offline.");
                                }
                            });
                }
            });
        }



       /* TextView desc=(TextView)findViewById(R.id.desc);

        if(!TextUtils.isEmpty(event.getlDesc())) {
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
                desc.setText(Html.fromHtml(event.getlDesc(), Html.FROM_HTML_MODE_LEGACY));
            } else {
                desc.setText(Html.fromHtml(event.getlDesc()));
            }
        }
        else
            desc.setText("oops ! No information.");

        desc.setVisibility(View.GONE);*/
        final WebView eventWeb=(WebView)findViewById(R.id.eventWeb);
        eventWeb.setVisibility(View.VISIBLE);
        if(!TextUtils.isEmpty(event.getlDesc())) {

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    eventWeb.setWebViewClient(new WebViewClient()
                                              {
                                                  public boolean shouldOverrideUrlLoading(WebView view, String url) {
                                                      if (url != null && (url.startsWith("http://") || url.startsWith("https://"))) {
                                                          view.getContext().startActivity(
                                                                  new Intent(Intent.ACTION_VIEW, Uri.parse(url)));
                                                          return true;
                                                      } else {
                                                          return false;
                                                      }
                                                  }
                                              }
                    );
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                        eventWeb.setLayerType(View.LAYER_TYPE_HARDWARE, null);
                    } else {
                        eventWeb.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
                    }

                    WebSettings settings=eventWeb.getSettings();
                    settings.setAppCacheEnabled(true);
                    settings.setCacheMode(WebSettings.LOAD_CACHE_ELSE_NETWORK);
                    eventWeb.loadData(event.getlDesc(),"text/html","utf-8");
                    eventWeb.setInitialScale(0);
                    settings.setLoadWithOverviewMode(true);
                    //settings.setUseWideViewPort(true);
                    settings.setBuiltInZoomControls(true);
                    settings.setSupportZoom(true);

                }
            });
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // action with ID action_refresh was selected
            case android.R.id.home:

                finish();
            default:
                break;
        }

        return true;
    }

}
