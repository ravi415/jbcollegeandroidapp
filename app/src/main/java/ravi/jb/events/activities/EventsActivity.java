package ravi.jb.events.activities;

import android.app.Activity;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import ravi.jb.JbApplication;
import ravi.jb.R;
import ravi.jb.events.adapters.RecyclerEventAdapter;
import ravi.jb.events.model.Event;
import ravi.jb.events.model.NetworkChangeEvent;
import ravi.jb.util.CommonUtil;
import ravi.jb.util.SQLiteManager;

public class EventsActivity extends AppCompatActivity {

    private static final String TAG=EventsActivity.class.getSimpleName();

    private FirebaseDatabase mFirebaseInstance;
    private RecyclerEventAdapter adapter;
    private List<Event> events=new ArrayList<>(1);
    private int pageSize=50;
    private SQLiteManager sqLiteManager;

    public View progressBar;
    private View loadIndicatorLayout;
    public TextView loadMessage;

    private Activity contextActivity;


    @Subscribe(threadMode = ThreadMode.MAIN,sticky = true)
    public void onMessageEvent(NetworkChangeEvent event)
    {
        setUp();
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_events);
        contextActivity=this;

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        sqLiteManager= JbApplication.sqLiteManager;

        progressBar=findViewById(R.id.progressBar);
        loadMessage=(TextView) findViewById(R.id.loadMessage);
        loadIndicatorLayout=findViewById(R.id.loadIndicatorLayout);


        if(JbApplication.configs.get("max.firedb.fetch.count")!=null)
            pageSize=Integer.parseInt(JbApplication.configs.get("max.firedb.fetch.count"));

        setUp();

    }

    private void setUp(){

        openDialog();
        if(events!=null && events.size()>0)
            Collections.sort(events,new EventComparatorByPriority());

        adapter=new RecyclerEventAdapter(events,contextActivity);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(contextActivity);

        RecyclerView recyclerView=(RecyclerView)findViewById(R.id.recycler_view);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(adapter);
        if(CommonUtil.isNetworkOnline(contextActivity)) {

            mFirebaseInstance = FirebaseDatabase.getInstance();
            //sqLiteManager.deleteAllFromEventArchive();

            if(events!=null)
                events.clear();
            else
                events=new ArrayList<>();

            Query query = mFirebaseInstance.getReference("events").orderByChild("priority").limitToFirst(pageSize);
            query.addChildEventListener(childEventListener);
        }
        else {

            if(events==null)
                events=new ArrayList<>();

            events.clear();
            Set<Event> eventSet=new LinkedHashSet<>();
            eventSet.addAll(sqLiteManager.getAllEventsFromArchive());
            events.addAll(eventSet);

        }

        notifyUI();

        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
            }

            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }
        });


    }

    private ChildEventListener childEventListener=new ChildEventListener() {
        @Override
        public void onChildAdded(DataSnapshot dataSnapshot, String s) {

            if(dataSnapshot!=null) {
                Event event = dataSnapshot.getValue(Event.class);
                if (event != null) {

                    int i=events.indexOf(event);
                    if(i!=-1) {
                        events.set(i,event);
                        sqLiteManager.updateEventArchive(event);
                    }
                    else{
                        events.add(0, event);
                        sqLiteManager.insertIntoEventArchive(event);
                    }

                    notifyUI();
                }
            }
        }

        @Override
        public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            if(dataSnapshot!=null) {
                Event event = dataSnapshot.getValue(Event.class);
                if (event != null) {
                    int i = events.indexOf(event);
                    if (i >= 0) {
                        events.set(i, event);
                        sqLiteManager.updateEventArchive(event);
                        notifyUI();
                    }

                }
            }
        }

        @Override
        public void onChildRemoved(DataSnapshot dataSnapshot) {

            if(dataSnapshot!=null) {
                Event event = dataSnapshot.getValue(Event.class);
                if (event != null) {
                    events.remove(event);
                    sqLiteManager.deleteFromEventArchive(event);

                    notifyUI();
                }
            }
        }

        @Override
        public void onChildMoved(DataSnapshot dataSnapshot, String s) {

        }

        @Override
        public void onCancelled(DatabaseError databaseError) {
            dismissProgressDialog();
            contextActivity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(contextActivity,"Error in connecting server.",Toast.LENGTH_SHORT).show();
                }
            });
        }
    };

    private void dismissProgressDialog(){
       /* if(progressDialog!=null && progressDialog.isShowing())
            progressDialog.dismiss();*/
        progressBar.setVisibility(View.INVISIBLE);
        loadIndicatorLayout.setVisibility(View.INVISIBLE);

    }

    private void notifyUI(){

        if(events!=null){
            Collections.sort(events,new EventComparatorByPriority());
        }
        contextActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                adapter.notifyDataSetChanged();
                dismissProgressDialog();
            }
        });
    }

    static class EventComparatorByPriority implements Comparator<Event> {

        @Override
        public int compare(Event o1, Event o2) {
            return o2.getPriority()-o1.getPriority();
        }
    }

    private void openDialog(){


        loadIndicatorLayout.setVisibility(View.VISIBLE);
        progressBar.setVisibility(View.VISIBLE);

    /*    progressDialog=new ProgressDialog(contextActivity);
        progressDialog.setMessage(contextActivity.getResources().getString(R.string.events_load_msg));
        progressDialog.show();
        progressDialog.setCancelable(false);
        progressDialog.setCanceledOnTouchOutside(false);
        new Handler().postDelayed(new Runnable() {
            public void run() {
                if(progressDialog!=null && progressDialog.isShowing()) {
                    dismissProgressDialog();
                    Toast.makeText(contextActivity,
                            "Couldn't connect, please try again later.", Toast.LENGTH_LONG).show();

                    ((MainActivity)contextActivity).startFragment(NoConnectivityFragment.newInstance(contextActivity)
                    ,contextActivity.getResources().getString(R.string.events_title));
                }
            }
        }, 5000);*/
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.refreshable_menu, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // action with ID action_refresh was selected
            case android.R.id.home:
                finish();
            case R.id.action_refresh:
                setUp();
                break;
            default:
                break;
        }

        return true;
    }

    public void onBackPressed() {
        finish();
    }





}
