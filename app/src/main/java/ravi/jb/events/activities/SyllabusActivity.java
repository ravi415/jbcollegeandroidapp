package ravi.jb.events.activities;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.TextView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.io.File;

import ravi.jb.JbApplication;
import ravi.jb.R;
import ravi.jb.events.model.NetworkChangeEvent;
import ravi.jb.events.model.SyllabusObj;
import ravi.jb.fcm.FirebaseStorageHandler;
import ravi.jb.util.CommonUtil;

public class SyllabusActivity extends AppCompatActivity {

    private static final String TAG=SyllabusActivity.class.getSimpleName();
    private  WebView mWebView;
    private TextView loadMessage;
    private View progressBar;

    private View loadIndicatorLayout;




    @Subscribe(threadMode = ThreadMode.MAIN,sticky = true)
    public void onMessageEvent(NetworkChangeEvent event)
    {
        if(!event.isOnline()){
            CommonUtil.showToastMessage(this,this.getResources().getString(R.string.usr_offline_msg));
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_syllabus);
        loadMessage=(TextView) findViewById(R.id.loadMessage);


        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        SyllabusObj.Semester semester= JbApplication.selectedSemester;
        Log.d(TAG,"++++++++"+semester);
        getSupportActionBar().setTitle(semester!=null && semester.getSemesterName()!=null?semester.getSemesterName():"Semester");



        mWebView = (WebView) findViewById(R.id.mWebView);
        progressBar = findViewById(R.id.progressBar);
        loadIndicatorLayout = findViewById(R.id.loadIndicatorLayout);
        mWebView.setVisibility(View.INVISIBLE);

        if(semester!=null) {

            mWebView.setVisibility(View.VISIBLE);
            File file = new File(CommonUtil.getExternalStorageFolder() + File.pathSeparator + CommonUtil.getEncodedFileName(semester.getPath()));

            Log.d(TAG,"For file:"+file.getAbsolutePath());

            if (file.exists())
                setmWebView(file.getAbsolutePath());
                //checkExternalStoragePermission();

            downloadFile();

        }
        else
        {
               loadMessage.setText("Whoa! something missing!");
               progressBar.setVisibility(View.GONE);
               mWebView.setVisibility(View.GONE);
        }

    }

    private void setmWebView(final String url){
        Log.d(TAG,"Loading url:"+url);
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mWebView.setWebViewClient(new WebViewClient() {
                    @Override
                    public void onReceivedError(WebView view, int errorCode,
                                                String description, String failingUrl) {
                        // Handle the error

                        Log.d(TAG,errorCode+">description>"+description+">"+failingUrl);
                        setProgressBar(false);
                    }

                    @Override
                    public boolean shouldOverrideUrlLoading(WebView view, String url) {

                        Log.d(TAG,"shouldOverrideUrlLoading:"+url);
                        return false;
                    }


                    @Override
                    public void onPageStarted(WebView view, String url, Bitmap favicon) {
                        super.onPageStarted(view, url, favicon);
                        Log.d(TAG,"Starting for :"+url);
                    }

                    @Override
                    public void onPageFinished(WebView view, String url) {
                        super.onPageFinished(view, url);
                        mWebView.setVisibility(View.VISIBLE);
                    }
                });
                WebSettings wbset=mWebView.getSettings();
                wbset.setJavaScriptEnabled(true);
                mWebView.setInitialScale(1);
                wbset.setLoadWithOverviewMode(true);
                wbset.setUseWideViewPort(true);

                wbset.setBuiltInZoomControls(true);
                wbset.setSupportZoom(true);


                String localUrl="file:///"+url;
                mWebView.loadUrl(localUrl);
                Log.d(TAG,"Loadidng Url:"+localUrl);
            }
        });
    }

    private void setProgressBar(boolean toShow){

        if(toShow){
            progressBar.setVisibility(View.VISIBLE);
            loadIndicatorLayout.setVisibility(View.VISIBLE);
        }
        else {
            progressBar.setVisibility(View.INVISIBLE);
            loadIndicatorLayout.setVisibility(View.INVISIBLE);
        }
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // action with ID action_refresh was selected
            case android.R.id.home:
                finish();
            default:
                break;
        }

        return true;
    }


    public void onBackPressed() {
        finish();
    }

    private void downloadFile(){


        FirebaseStorageHandler.instance(new FirebaseStorageHandler.FileDownloadStatus() {
            @Override
            public void onSuccess(String localUrl) {

                Log.d(TAG,"Locally saved path:"+localUrl);
               setmWebView(localUrl);
            }

            @Override
            public void onError(String message) {

                Log.e(TAG,"Failed to download file :"+message);
            }
        }).downloadFile(JbApplication.selectedSemester.getPath());
    }
}
