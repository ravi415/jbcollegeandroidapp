package ravi.jb.events.activities;

import android.app.Activity;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.google.gson.Gson;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import ravi.jb.JbApplication;
import ravi.jb.R;
import ravi.jb.events.adapters.RecyclerGalleryCatAdapter;
import ravi.jb.events.model.GalleryObj;
import ravi.jb.events.model.NetworkChangeEvent;
import ravi.jb.util.CommonUtil;

public class GalleryCatActivity extends AppCompatActivity {

    private static final String TAG=GalleryCatActivity.class.getSimpleName();

    public View progressBar;
    private View loadIndicatorLayout;
    public TextView loadMessage;
    private GalleryObj galleryObj;
    private Activity contextActivity;
    private RecyclerGalleryCatAdapter adapter;


    @Subscribe(threadMode = ThreadMode.MAIN,sticky = true)
    public void onMessageEvent(NetworkChangeEvent event)
    {
       setUp();
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news);
        contextActivity=this;

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);



        progressBar=findViewById(R.id.progressBar);
        loadMessage=(TextView) findViewById(R.id.loadMessage);
        loadIndicatorLayout=findViewById(R.id.loadIndicatorLayout);

        setUp();

    }

    private void setUp(){

        openDialog();

        try {

            if(CommonUtil.isNetworkOnline(contextActivity)) {
                galleryObj = (new Gson()).fromJson(JbApplication.configs.get("gallery"), GalleryObj.class);
                adapter = new RecyclerGalleryCatAdapter(galleryObj.getGalleryItems(), contextActivity);
                RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(contextActivity, 2);

                RecyclerView recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
                recyclerView.setLayoutManager(mLayoutManager);
                recyclerView.setItemAnimator(new DefaultItemAnimator());
                recyclerView.setAdapter(adapter);
            }
            else
                CommonUtil.showOfflineMsg(contextActivity);
        }catch(Exception e){

            Log.d(TAG,""+e.getMessage());
        }finally {
            dismissProgressDialog();
        }

    }

    private void dismissProgressDialog(){
       /* if(progressDialog!=null && progressDialog.isShowing())
            progressDialog.dismiss();*/
        progressBar.setVisibility(View.INVISIBLE);
        loadIndicatorLayout.setVisibility(View.INVISIBLE);

    }


    private void openDialog(){

        loadIndicatorLayout.setVisibility(View.VISIBLE);
        progressBar.setVisibility(View.VISIBLE);

    }


    public void onBackPressed() {
        finish();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.refreshable_menu, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // action with ID action_refresh was selected
            case android.R.id.home:
                finish();
            case R.id.action_refresh:
                setUp();
                break;
            default:
                break;
        }

        return true;
    }

}
