package ravi.jb.events.activities;

import android.app.Activity;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import ravi.jb.JbApplication;
import ravi.jb.R;
import ravi.jb.events.adapters.RecyclerNewsAdapter;
import ravi.jb.events.model.NetworkChangeEvent;
import ravi.jb.events.model.NewsElement;
import ravi.jb.util.CommonUtil;
import ravi.jb.util.SQLiteManager;

public class NewsActivity extends AppCompatActivity {

    private static final String TAG=NewsActivity.class.getSimpleName();

    private FirebaseDatabase mFirebaseInstance;
    private String dbName="news";
    private RecyclerNewsAdapter adapter;
    private List<NewsElement> newsElements=new ArrayList<>(1);

    private int pageSize=100;

    private SQLiteManager sqLiteManager;
    public View progressBar;
    private View loadIndicatorLayout;
    public TextView loadMessage;

    private Activity contextActivity;


    @Subscribe(threadMode = ThreadMode.MAIN,sticky = true)
    public void onMessageEvent(NetworkChangeEvent event)
    {
       setUp();
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news);
        contextActivity=this;

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        sqLiteManager= JbApplication.sqLiteManager;


        progressBar=findViewById(R.id.progressBar);
        loadMessage=(TextView) findViewById(R.id.loadMessage);
        loadIndicatorLayout=findViewById(R.id.loadIndicatorLayout);

        if(JbApplication.configs.get("max.firedb.fetch.count")!=null)
            pageSize=Integer.parseInt(JbApplication.configs.get("max.firedb.fetch.count"));

        setUp();

    }

    private void setUp(){


        openDialog();
        if(newsElements!=null && newsElements.size()>0)
            Collections.sort(newsElements,new NewsElementComparatorByPriority());

        adapter=new RecyclerNewsAdapter(newsElements,contextActivity);
        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(contextActivity,2);

        RecyclerView recyclerView=(RecyclerView)findViewById(R.id.recycler_view);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(adapter);
        if(CommonUtil.isNetworkOnline(contextActivity)) {
            mFirebaseInstance = FirebaseDatabase.getInstance();

            sqLiteManager.deleteAllFromNewsArchive();
            if(newsElements!=null)
                newsElements.clear();
            else
                newsElements=new ArrayList<>();


            Query query = mFirebaseInstance.getReference(dbName).orderByChild("priority").limitToFirst(pageSize);
            query.addChildEventListener(childEventListener);
        }
        else {

            if(newsElements==null)
                newsElements=new ArrayList<>();

            newsElements.clear();
            Set<NewsElement> elementSet=new LinkedHashSet<>();
            elementSet.addAll(sqLiteManager.getAllNewsFromArchive());
            newsElements.addAll(elementSet);
        }

        notifyUI();

        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
            }

            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }
        });


    }

    private ChildEventListener childEventListener=new ChildEventListener() {
        @Override
        public void onChildAdded(DataSnapshot dataSnapshot, String s) {

            if(dataSnapshot!=null) {
                NewsElement event = dataSnapshot.getValue(NewsElement.class);
                if (event != null) {

                    int i=newsElements.indexOf(event);
                    if(i!=-1) {
                        newsElements.set(i,event);
                        sqLiteManager.updateNewsArchive(event);
                    }
                    else {
                        newsElements.add(0, event);
                        sqLiteManager.insertIntoNewsArchive(event);
                    }
                    notifyUI();
                }
            }
        }

        @Override
        public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            if(dataSnapshot!=null) {
                NewsElement event = dataSnapshot.getValue(NewsElement.class);
                if (event != null) {

                    int i = newsElements.indexOf(event);
                    if (i >= 0) {
                        newsElements.set(i, event);
                        sqLiteManager.updateNewsArchive(event);
                        notifyUI();

                    }

                }
            }
        }

        @Override
        public void onChildRemoved(DataSnapshot dataSnapshot) {

            if(dataSnapshot!=null) {
                NewsElement event = dataSnapshot.getValue(NewsElement.class);
                if (event != null) {
                    newsElements.remove(event);
                    sqLiteManager.deleteFromNewsArchive(event);
                    notifyUI();
                }
            }
        }

        @Override
        public void onChildMoved(DataSnapshot dataSnapshot, String s) {

        }

        @Override
        public void onCancelled(DatabaseError databaseError) {
            dismissProgressDialog();
            contextActivity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(contextActivity,"Error in connecting server.",Toast.LENGTH_SHORT).show();
                }
            });
        }
    };

    private void dismissProgressDialog(){
       /* if(progressDialog!=null && progressDialog.isShowing())
            progressDialog.dismiss();*/
        progressBar.setVisibility(View.INVISIBLE);
        loadIndicatorLayout.setVisibility(View.INVISIBLE);

    }

    private void notifyUI(){

        if(newsElements!=null){
            Collections.sort(newsElements,new NewsElementComparatorByPriority());
        }
        contextActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                adapter.notifyDataSetChanged();
                dismissProgressDialog();
            }
        });
    }

    static class NewsElementComparatorByPriority implements Comparator<NewsElement> {

        @Override
        public int compare(NewsElement o1, NewsElement o2) {
            return o2.getPriority()-o1.getPriority();
        }
    }

    private void openDialog(){

        loadIndicatorLayout.setVisibility(View.VISIBLE);
        progressBar.setVisibility(View.VISIBLE);

    }


    public void onBackPressed() {
        finish();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.refreshable_menu, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // action with ID action_refresh was selected
            case android.R.id.home:
                finish();
            case R.id.action_refresh:
                setUp();
                break;
            default:
                break;
        }

        return true;
    }

}
