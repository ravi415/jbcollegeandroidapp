package ravi.jb.events.adapters;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import ravi.jb.JbApplication;
import ravi.jb.R;
import ravi.jb.events.activities.SyllabusActivity;
import ravi.jb.events.model.SyllabusObj;

/**
 * Created by brakisho on 2/9/2017.
 */

public class RecyclerSyllabusAdapter extends RecyclerView.Adapter<RecyclerSyllabusAdapter.MyViewHolder> {

    private static final String TAG=RecyclerSyllabusAdapter.class.getSimpleName();
    int[] syllabiColors = null;

    private List<SyllabusObj> syllabusObjs;
    private Context context;

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public TextView name;
        public View rootView;
        public MyViewHolder(View view) {
            super(view);
            name = (TextView) view.findViewById(R.id.name);
            rootView=view;
            view.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {

            SyllabusObj syllabusObj=syllabusObjs.get(getAdapterPosition());
            if(syllabusObj!=null && syllabusObj.getSemesters()!=null &&syllabusObj.getSemesters().size()>0){
            AlertDialog.Builder builderSingle = new AlertDialog.Builder(context);
            builderSingle.setIcon(R.drawable.logo);
            builderSingle.setTitle("Choose Semester");

            final ArrayAdapter<SyllabusObj.Semester> arrayAdapter = new ArrayAdapter<>(context, android.R.layout.select_dialog_singlechoice);

            for(SyllabusObj.Semester semester:syllabusObj.getSemesters())
                arrayAdapter.add(semester);

            builderSingle.setNegativeButton("cancel", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });

            builderSingle.setAdapter(arrayAdapter, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    SyllabusObj.Semester semester = arrayAdapter.getItem(which);
                    JbApplication.selectedSemester=semester;
                    Log.d(TAG,"selected semester:"+semester.getSemesterName());

                    Intent intent=new Intent(context, SyllabusActivity.class);
                    context.startActivity(intent);
                }
            });
            builderSingle.show();




        }
        }
    }


    public RecyclerSyllabusAdapter(List<SyllabusObj> syllabusObjs, Context context) {
        this.syllabusObjs = syllabusObjs;
        this.context=context;
        syllabiColors=context.getResources().getIntArray(R.array.colorArray);
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.syllabi_recycler_row, parent, false);

            return new MyViewHolder(itemView);


    }


    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {

        final SyllabusObj syllabusObj = syllabusObjs.get(position);

        String courseName=syllabusObj.getCourceName();

            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
                holder.name.setText(Html.fromHtml(courseName, Html.FROM_HTML_MODE_LEGACY));
            } else {
                holder.name.setText(Html.fromHtml(courseName));
            }

        holder.rootView.setBackgroundColor(syllabiColors[position%syllabiColors.length]);

    }

    @Override
    public int getItemCount() {
        return syllabusObjs.size();
    }


}