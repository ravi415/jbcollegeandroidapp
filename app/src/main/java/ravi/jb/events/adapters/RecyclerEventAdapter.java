package ravi.jb.events.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import ravi.jb.JbApplication;
import ravi.jb.R;
import ravi.jb.events.activities.ScrollingEventDetailsActivity;
import ravi.jb.events.model.Event;

/**
 * Created by brakisho on 2/9/2017.
 */

public class RecyclerEventAdapter extends RecyclerView.Adapter<RecyclerEventAdapter.MyViewHolder> {

    private static final String TAG=RecyclerEventAdapter.class.getSimpleName();

    private List<Event> events;
    private Context context;

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public TextView name, desc;
        public ImageView imgView;
        public MyViewHolder(View view) {
            super(view);
            name = (TextView) view.findViewById(R.id.name);
            desc = (TextView) view.findViewById(R.id.desc);
            imgView=(ImageView) view.findViewById(R.id.imgView);
            view.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {

            Activity a=context instanceof Activity ?(Activity)context:null;
            if(a!=null) {
                JbApplication.selectedEvent=events.get(getLayoutPosition());
                Intent i = new Intent(a, ScrollingEventDetailsActivity.class);
                a.startActivity(i);
            }
            else
                JbApplication.selectedEvent=null;


        }
    }


    public RecyclerEventAdapter(List<Event> events,Context context) {
        this.events = events;
        this.context=context;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.m_recycler_row, parent, false);

            return new MyViewHolder(itemView);


    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {

        final Event event = events.get(position);

        String eventName=event.getName();

            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
                holder.name.setText(Html.fromHtml(eventName, Html.FROM_HTML_MODE_LEGACY));
            } else {
                holder.name.setText(Html.fromHtml(eventName));
            }


        String desc=event.getsDesc();

            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
                holder.desc.setText(Html.fromHtml(desc, Html.FROM_HTML_MODE_LEGACY));
            } else {
                holder.desc.setText(Html.fromHtml(desc));
            }

        
        if(holder.imgView!=null){

            if(event!=null && event.getImgUrl()!=null && !TextUtils.isEmpty(event.getImgUrl())) {
                Log.d(TAG,"Getting:"+event.getImgUrl());
                ((Activity) context).runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Picasso picasso = Picasso.with(context);
                        picasso.load(event.getImgUrl())
                                .resize(80, 80)
                                .into(holder.imgView);
                    }
                });
            }
            else
                holder.imgView.setImageResource(android.R.drawable.ic_menu_gallery);
        }
        else{
            holder.imgView.setImageResource(android.R.drawable.ic_menu_gallery);
        }
    }

    @Override
    public int getItemCount() {
        return events.size();
    }


}