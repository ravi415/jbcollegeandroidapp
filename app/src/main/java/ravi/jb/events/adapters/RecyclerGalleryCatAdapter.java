package ravi.jb.events.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import ravi.jb.JbApplication;
import ravi.jb.R;
import ravi.jb.actvities.GalleryActivity;
import ravi.jb.events.model.GalleryItem;


public class RecyclerGalleryCatAdapter extends RecyclerView.Adapter<RecyclerGalleryCatAdapter.MyViewHolder> {

    private static final String TAG=RecyclerGalleryCatAdapter.class.getSimpleName();

    private List<GalleryItem> galleryItems;
    private Context context;
    private List<Integer> colors=new ArrayList<>();

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public TextView name;
        public ImageView imgView;
        public MyViewHolder(View view) {
            super(view);
            name = (TextView) view.findViewById(R.id.name);
            imgView=(ImageView) view.findViewById(R.id.imgView);
            view.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {

            try {
                Activity a = context instanceof Activity ? (Activity) context : null;
                if (a != null) {
                    JbApplication.galleryItem = galleryItems.get(getLayoutPosition());

                    Intent i = new Intent(a, GalleryActivity.class);
                    i.putExtra("CATEGORY",galleryItems.get(getLayoutPosition()).getCategory());
                    a.startActivity(i);
                } else
                    JbApplication.galleryItem = null;
            } catch (Exception e) {
                JbApplication.galleryItem = null;
                Log.d(TAG, "" + e.getMessage());
            }
        }
    }


    public RecyclerGalleryCatAdapter(List<GalleryItem> galleryItems, Context context) {
        this.galleryItems = galleryItems;
        this.context=context;

        int s[]=context.getResources().getIntArray(R.array.colorArray);
        for(int i:s)
            colors.add(i);

        Collections.shuffle(colors);
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.gallerycat_row, parent, false);

            return new MyViewHolder(itemView);


    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {


        try {

            final GalleryItem item = galleryItems.get(position);

            String catName = item.getCategory();

            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
                holder.name.setText(Html.fromHtml(catName, Html.FROM_HTML_MODE_LEGACY));
            } else {
                holder.name.setText(Html.fromHtml(catName));
            }


            if (holder.imgView != null) {

                if (item != null && !TextUtils.isEmpty(item.getBasePath()) && !TextUtils.isEmpty(item.getCatImgName())) {
                    Log.d(TAG, "Getting:" + item.getCatImgName());
                    ((Activity) context).runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Picasso picasso = Picasso.with(context);
                            picasso.load(item.getBasePath()+item.getCatImgName())
                                    .resize(80, 80)
                                    .into(holder.imgView);
                        }
                    });
                } else
                    holder.imgView.setImageDrawable(null);
            } else {
                holder.imgView.setImageDrawable(null);
            }

        }catch (Exception e){
            Log.e(TAG,e.getMessage()+"");
        }
    }

    @Override
    public int getItemCount() {
        return galleryItems.size();
    }


}