package ravi.jb.events.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import ravi.jb.JbApplication;
import ravi.jb.R;
import ravi.jb.events.activities.ScrollingNewsDetailsActivity;
import ravi.jb.events.model.NewsElement;

/**
 * Created by brakisho on 2/9/2017.
 * Created by brakisho on 2/9/2017.
 */

public class RecyclerNewsAdapter extends RecyclerView.Adapter<RecyclerNewsAdapter.MyViewHolder> {

    private static final String TAG=RecyclerNewsAdapter.class.getSimpleName();

    private List<NewsElement> newsElements;
    private Context context;
    private List<Integer> colors=new ArrayList<>();

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public TextView name, desc;
        public ImageView imgView;
        public MyViewHolder(View view) {
            super(view);
            name = (TextView) view.findViewById(R.id.name);
            desc = (TextView) view.findViewById(R.id.desc);
            imgView=(ImageView) view.findViewById(R.id.imgView);
            view.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {

            Activity a=context instanceof Activity ?(Activity)context:null;
            if(a!=null) {
                JbApplication.selectedNews=newsElements.get(getLayoutPosition());
                Intent i = new Intent(a, ScrollingNewsDetailsActivity.class);
                a.startActivity(i);
            }
            else
                JbApplication.selectedNews=null;
        }
    }


    public RecyclerNewsAdapter(List<NewsElement> newsElements, Context context) {
        this.newsElements = newsElements;
        this.context=context;

        int s[]=context.getResources().getIntArray(R.array.colorArray);
        for(int i:s)
            colors.add(i);

        Collections.shuffle(colors);
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.news_row, parent, false);

            return new MyViewHolder(itemView);


    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {


        try {

            final NewsElement event = newsElements.get(position);

            String eventName = event.getName();

            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
                holder.name.setText(Html.fromHtml(eventName, Html.FROM_HTML_MODE_LEGACY));
            } else {
                holder.name.setText(Html.fromHtml(eventName));
            }


            String desc = event.getsDesc();

            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
                holder.desc.setText(Html.fromHtml(desc, Html.FROM_HTML_MODE_LEGACY));
            } else {
                holder.desc.setText(Html.fromHtml(desc));
            }


            if (holder.imgView != null) {

                if (event != null && event.getImgUrl() != null && !TextUtils.isEmpty(event.getImgUrl())) {
                    Log.d(TAG, "Getting:" + event.getImgUrl());
                    ((Activity) context).runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Picasso picasso = Picasso.with(context);
                            picasso.load(event.getImgUrl())
                                    .resize(80, 80)
                                    .into(holder.imgView);
                        }
                    });
                } else
                    holder.imgView.setImageResource(android.R.drawable.ic_menu_gallery);
            } else {
                holder.imgView.setImageResource(android.R.drawable.ic_menu_gallery);
            }

            //holder.imgView.setColorFilter(colors.get(position%colors.size()), PorterDuff.Mode.LIGHTEN);

        }catch (Exception e){
            Log.e(TAG,e.getMessage()+"");
        }
    }

    @Override
    public int getItemCount() {
        return newsElements.size();
    }


}